<?
class Variable {
	public static function validate () {
		switch ($GLOBALS['ctrl_name']) {
			case 'Streamingprovider' :
				$check = 0;
				$check += (!preg_match("/^[\w]{3,10}$/", @$GLOBALS['json_object']->appid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,30}$/", @$GLOBALS['json_object']->uid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->sessionid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{3,7}$/", @$GLOBALS['json_object']->channelid)) ? 1 : 0;
				// $check += (!preg_match("/^(th|en)$/", @$GLOBALS['json_object']->langid)) ? 1 : 0;
				$check += (!preg_match("/^(auto|hd|high|medium|low)$/", @$GLOBALS['json_object']->streamlvl)) ? 1 : 0;
				$check += (!preg_match("/^(live|timeshift|catchup)$/", @$GLOBALS['json_object']->type)) ? 1 : 0;
				$check += (!preg_match("/^(stb|web|mobile)$/", @$GLOBALS['json_object']->visitor)) ? 1 : 0;
				$GLOBALS['json_object']->uid = (empty($GLOBALS['json_object']->uid) ? self::generateRandomString() : $GLOBALS['json_object']->uid);
				$GLOBALS['json_object']->sessionid = (empty($GLOBALS['json_object']->sessionid) ? self::generateRandomString() : $GLOBALS['json_object']->sessionid);
				$GLOBALS['json_object']->csip = (empty($GLOBALS['json_object']->csip) ? self::generateRandomString() : $GLOBALS['json_object']->csip);
				return ($check == 0) ? true : false;
			break;
			case 'Streamingprovider2' :
				$check = 0;
				$check += (!preg_match("/^[\w]{3,10}$/", @$GLOBALS['json_object']->appid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,30}$/", @$GLOBALS['json_object']->uid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->sessionid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{3,7}$/", @$GLOBALS['json_object']->channelid)) ? 1 : 0;
				// $check += (!preg_match("/^(th|en)$/", @$GLOBALS['json_object']->langid)) ? 1 : 0;
				$check += (!preg_match("/^(auto|hd|high|medium|low)$/", @$GLOBALS['json_object']->streamlvl)) ? 1 : 0;
				$check += (!preg_match("/^(live|timeshift|catchup)$/", @$GLOBALS['json_object']->type)) ? 1 : 0;
				$check += (!preg_match("/^(stb|web|mobile|chromecast|tv|antv)$/", @$GLOBALS['json_object']->visitor)) ? 1 : 0;
				$GLOBALS['json_object']->uid = (empty($GLOBALS['json_object']->uid) ? self::generateRandomString() : $GLOBALS['json_object']->uid);
				$GLOBALS['json_object']->sessionid = (empty($GLOBALS['json_object']->sessionid) ? self::generateRandomString() : $GLOBALS['json_object']->sessionid);
				$GLOBALS['json_object']->csip = (empty($GLOBALS['json_object']->csip) ? self::generateRandomString() : $GLOBALS['json_object']->csip);
				return ($check == 0) ? true : false;
			break;
			case 'Priviledgestreamprovider' :
				$check = 0;
				$check += (!preg_match("/^[\w]{3,10}$/", @$GLOBALS['json_object']->appid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,30}$/", @$GLOBALS['json_object']->uid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->sessionid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{3,7}$/", @$GLOBALS['json_object']->channelid)) ? 1 : 0;
				// $check += (!preg_match("/^(th|en)$/", @$GLOBALS['json_object']->langid)) ? 1 : 0;
				$check += (!preg_match("/^(auto|hd|high|medium|low)$/", @$GLOBALS['json_object']->streamlvl)) ? 1 : 0;
				$check += (!preg_match("/^(live|timeshift|catchup)$/", @$GLOBALS['json_object']->type)) ? 1 : 0;
				$check += (!preg_match("/^(stb|web|mobile)$/", @$GLOBALS['json_object']->visitor)) ? 1 : 0;
				$GLOBALS['json_object']->uid = (empty($GLOBALS['json_object']->uid) ? self::generateRandomString() : $GLOBALS['json_object']->uid);
				$GLOBALS['json_object']->sessionid = (empty($GLOBALS['json_object']->sessionid) ? self::generateRandomString() : $GLOBALS['json_object']->sessionid);
				$GLOBALS['json_object']->csip = (empty($GLOBALS['json_object']->csip) ? self::generateRandomString() : $GLOBALS['json_object']->csip);
				return ($check == 0) ? true : false;
			break;
			case 'Radiostreamprovider' :
				$check = 0;
				$check += (!preg_match("/^[\w]{3,10}$/", @$GLOBALS['json_object']->appid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,30}$/", @$GLOBALS['json_object']->uid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{3,10}$/", @$GLOBALS['json_object']->channelid)) ? 1 : 0;
				$check += (!preg_match("/^(web|wap|mobile)$/", @$GLOBALS['json_object']->visitor)) ? 1 : 0;
		$GLOBALS['json_object']->uid = (empty(trim($GLOBALS['json_object']->uid)) ? self::generateRandomString() : $GLOBALS['json_object']->uid);
		$GLOBALS['json_object']->csip = (empty(trim($GLOBALS['json_object']->csip)) ? self::generateRandomString() : $GLOBALS['json_object']->csip);
		$GLOBALS['json_object']->charge = true;
				return ($check == 0) ? true : false;
			break;
			case 'Eventstreamprovider' :
				$check = 0;
				$check += (!preg_match("/^[\w]{3,10}$/", @$GLOBALS['json_object']->appid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->uid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->sessionid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{3,30}$/", @$GLOBALS['json_object']->programid)) ? 1 : 0;
				// $check += (!preg_match("/^(th|en)$/", @$GLOBALS['json_object']->langid)) ? 1 : 0;
				$check += (!preg_match("/^(auto|hd|high|medium|low)$/", @$GLOBALS['json_object']->streamlvl)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{3,10}$/", @$GLOBALS['json_object']->programstate)) ? 1 : 0;
				$check += (!preg_match("/^(stb|web|mobile)$/", @$GLOBALS['json_object']->visitor)) ? 1 : 0;
				$GLOBALS['json_object']->uid = (empty($GLOBALS['json_object']->uid) ? self::generateRandomString() : $GLOBALS['json_object']->uid);
				$GLOBALS['json_object']->sessionid = (empty($GLOBALS['json_object']->sessionid) ? self::generateRandomString() : $GLOBALS['json_object']->sessionid);
				$GLOBALS['json_object']->csip = (empty($GLOBALS['json_object']->csip) ? self::generateRandomString() : $GLOBALS['json_object']->csip);
				return ($check == 0) ? true : false;
			break;
			case 'Vodstreamprovider' :
				$check = 0;
				$check += (!preg_match("/^[\w]{3,20}$/", @$GLOBALS['json_object']->appid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->uid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->sessionid)) ? 1 : 0;
				// $check += (!preg_match("/^(th|en)$/", @$GLOBALS['json_object']->langid)) ? 1 : 0;
				$check += (!preg_match("/^(stb|web|mobile|chromecast|tv)$/", @$GLOBALS['json_object']->visitor)) ? 1 : 0;
				$check += (!preg_match("/^(auto|hd|high|medium|low)$/", @$GLOBALS['json_object']->streamlvl)) ? 1 : 0;
				$GLOBALS['json_object']->uid = (empty($GLOBALS['json_object']->uid) ? self::generateRandomString() : $GLOBALS['json_object']->uid);
				$GLOBALS['json_object']->sessionid = (empty($GLOBALS['json_object']->sessionid) ? self::generateRandomString() : $GLOBALS['json_object']->sessionid);
				$GLOBALS['json_object']->csip = (empty($GLOBALS['json_object']->csip) ? self::generateRandomString() : $GLOBALS['json_object']->csip);
				return ($check == 0) ? true : false;
			break;
			case 'Vodstreamprovider_test' :
				$check = 0;
				$check += (!preg_match("/^[\w]{3,20}$/", @$GLOBALS['json_object']->appid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->uid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->sessionid)) ? 1 : 0;
				// $check += (!preg_match("/^(th|en)$/", @$GLOBALS['json_object']->langid)) ? 1 : 0;
				$check += (!preg_match("/^(stb|web|mobile|chromecast)$/", @$GLOBALS['json_object']->visitor)) ? 1 : 0;
				$check += (!preg_match("/^(auto|hd|high|medium|low)$/", @$GLOBALS['json_object']->streamlvl)) ? 1 : 0;
				$GLOBALS['json_object']->uid = (empty($GLOBALS['json_object']->uid) ? self::generateRandomString() : $GLOBALS['json_object']->uid);
				$GLOBALS['json_object']->sessionid = (empty($GLOBALS['json_object']->sessionid) ? self::generateRandomString() : $GLOBALS['json_object']->sessionid);
				$GLOBALS['json_object']->csip = (empty($GLOBALS['json_object']->csip) ? self::generateRandomString() : $GLOBALS['json_object']->csip);
				return ($check == 0) ? true : false;
			break;
			// case 'Vodstreamprovider' :
				// $check = 0;
				// $check += (!preg_match("/^[\w]{3,20}$/", @$GLOBALS['json_object']->appid)) ? 1 : 0;
				// $check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->uid)) ? 1 : 0;
				// $check += (!preg_match("/^[\w]{0,15}$/", @$GLOBALS['json_object']->sessionid)) ? 1 : 0;
				// $check += (!preg_match("/^(th|en)$/", @$GLOBALS['json_object']->langid)) ? 1 : 0;
				// $check += (!preg_match("/^(stb|web|mobile|chromecast)$/", @$GLOBALS['json_object']->visitor)) ? 1 : 0;
				// $check += (!preg_match("/^(auto|hd|high|medium|low)$/", @$GLOBALS['json_object']->streamlvl)) ? 1 : 0;
				// $GLOBALS['json_object']->uid = (empty($GLOBALS['json_object']->uid) ? self::generateRandomString() : $GLOBALS['json_object']->uid);
				// $GLOBALS['json_object']->sessionid = (empty($GLOBALS['json_object']->sessionid) ? self::generateRandomString() : $GLOBALS['json_object']->sessionid);
				// $GLOBALS['json_object']->csip = (empty($GLOBALS['json_object']->csip) ? self::generateRandomString() : $GLOBALS['json_object']->csip);
				// return ($check == 0) ? true : false;
			// break;
			case 'Tvodstreamprovider' :
				$check = 0;
				$check += (!preg_match("/^[\w]{3,20}$/", @$GLOBALS['json_object']->appid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{1,30}$/", @$GLOBALS['json_object']->uid)) ? 1 : 0;
				$check += (!preg_match("/^[\w]{1,15}$/", @$GLOBALS['json_object']->sessionid)) ? 1 : 0;
				// $check += (!preg_match("/^(th|en)$/", @$GLOBALS['json_object']->langid)) ? 1 : 0;
				$check += (!preg_match("/^(auto|hd|high|medium|low)$/", @$GLOBALS['json_object']->streamlvl)) ? 1 : 0;
				$check += (!preg_match("/^(stb|web|mobile|chromecast|tv)$/", @$GLOBALS['json_object']->visitor)) ? 1 : 0;
				$check += (!preg_match("/^[a-zA-Z0-9 ,._-]{1,30}$/", @$GLOBALS['json_object']->agent)) ? 1 : 0;
				$check += (!preg_match("/^(wv|fp)$/", @$GLOBALS['json_object']->drm)) ? 1 : 0;
				$GLOBALS['json_object']->csip = (empty($GLOBALS['json_object']->csip) ? self::generateRandomString() : $GLOBALS['json_object']->csip);
				return ($check == 0) ? true : false;
			break;
			default :
				# undefined controller
				return false;
			break;
		}
	}
	
	private static function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	} 
}
