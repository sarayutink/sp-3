<?
class Arsopensslcryption {
	public static function encrypt ($strtoencrypt) {
		$iv = openssl_random_pseudo_bytes(16);
		$encryption_key = 'TDMP-010D9444';
		$data = openssl_encrypt($strtoencrypt, 'aes-128-cbc', $encryption_key, OPENSSL_RAW_DATA , $iv);
		return str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($data.":+A:+R:+S:".$iv));
	}
	
	public static function encryptbypass () {
		$token_arr = array('aDR81TY2wbl5G3bD', 'o54YDHDpHOLxlDl0', 'mTPiMyHNGwGIjEqY', 'EjixkniGQJ51ufjD', 'gUdwhojxwe5MrdbO');
		
		$iv = openssl_random_pseudo_bytes(16);
		$encryption_key = 'TDMP-010D9444';

		$data = openssl_encrypt($token_arr[rand(0,count($token_arr)-1)], 'aes-128-cbc', $encryption_key, OPENSSL_RAW_DATA , $iv);
		
		return str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($data.":+A:+R:+S:".$iv));
	}
	
	public static function decrypt ($strtodecrypt) {
		$encryption_key = 'TDMP-010D9444';
		$strtodecrypt = explode(":+A:+R:+S:",base64_decode(str_replace(array('-', '_' , ''),array('+', '/', '='),$strtodecrypt)));
		return openssl_decrypt($strtodecrypt[0], 'aes-128-cbc', $encryption_key, OPENSSL_RAW_DATA , $strtodecrypt[1]);
	}
}