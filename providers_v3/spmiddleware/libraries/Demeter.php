<?php
class Demeter {
        private $redisClient; private $cdnsdesc;

        public function findCDN ($groupname) {
            $host = "10.18.19.99";
            $port = 6383;
            $db = 8;
            $return = false;
            try {
                $redis = new Redis();
                $redis->connect($host, $port);
                $redis->select($db);
                $this->cdnsdesc = $redis->get($groupname);
                $redis->close();
            } catch (Exception $e) {
                file_put_contents(__DIR__."/cdnselector-err.log", date("Y-m-d_H:i:s", time())."  ".$host.":".$port.":".$db."  ".$e->getMessage()."\n", FILE_APPEND);
            }
            $cdns_arr = json_decode($this->cdnsdesc, true);
            switch (count($cdns_arr)) {
                case 0:
                    $return = false;
                    break;
                case 1 :
                    $return = $cdns_arr[0]['host'];
                    break;
                case 2 :
                    $return = $cdns_arr[array_rand($cdns_arr)]['host'];
                    break;
                default :
                    $samples = array_rand($cdns_arr, 2);
                    $return = $cdns_arr[$samples[0]]['ccu_available'] > $cdns_arr[$samples[1]]['ccu_available'] ? $cdns_arr[$samples[0]]['host'] : $cdns_arr[$samples[1]]['host'];
                    break;
            }
            return $return;
        }
}
