<?
require_once("RedisHandler.php");

class PrefixLoader {
	public function loadPrefix ($prefix_num, $filename) {
		$handle = fopen("$filename", "r") or die("Couldn't get handle");
		if ($handle) {
			$tree = array();
    		while (!feof($handle)) {
        		$buffer = fgets($handle, 4096);
				// $cols = explode("\t", $buffer);
				// $cols = explode(" ", $buffer);
				preg_match("~^(?:[0-9]{1,3}\.){3}[0-9]{1,3}/[0-9][0-9]~", $buffer, $cols);
				$subnets = explode("/", $cols[0]);
				$octets = explode(".", $subnets[0]);
				@$tree[$octets[0]][$octets[1]][$octets[2]][$octets[3]][$prefix_num] = $octets[0].".".$octets[1].".".$octets[2].".".$octets[3]."/".$subnets[1];
    		}
    		fclose($handle);
			return ($tree);
		}
	}
}

class PrefixSearch {
	private $tree;
	private $cip;
	private $hit;

	private $starttime;
	private $endtime;

	public function __construct () {
		$this->hit = false;
	}

	public function loadPrefixes ($tree) {
		$this->tree = $tree;
	}

	public function startExecution () {
		$this->starttime = microtime();
	}


	public function endExecution () {
		$this->endtime = microtime();
	}

	public function countExecution () {
		return $this->endtime - $this->starttime;
	}

	private function cidr_match ($ip, $range) {
    	list ($subnet, $bits) = explode('/', $range);
    	$ip = ip2long($ip);
    	$subnet = ip2long($subnet);
    	$mask = -1 << (32 - $bits);
    	$subnet &= $mask;
    	return ($ip & $mask) == $subnet;
	}

	private function searchRoute ($item, $zone) {
		if ($this->cidr_match($this->cip, $item) === true) $this->hit = $zone;
	}

	public function searchThroughPrefix ($cip) {
		$cols = explode(".", $cip);
		$this->cip = $cip;
		if (isset($this->tree[$cols[0]])) array_walk_recursive($this->tree[$cols[0]], array($this,'searchRoute'));
		return  ($this->hit? $this->hit : 1);
	}

	private function searchRouteWithChoose ($item, $zone) {
		if ($this->cidr_match($this->cip, $item) === true) $this->hit[] = $item ."::". $zone;
	}

	public function searchThroughPrefixWithChoose ($cip) {
		$this->cip = $cip;
		array_walk_recursive($this->tree, array($this,'searchRouteWithChoose'));
		return  ($this->hit? $this->hit : 0);
	}
}