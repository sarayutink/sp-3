<?
require_once("RedisHandler.php");

class CDNZone {
	public function setCDNZone ($cdnzone_num, $cdnzone_arr) {
		RedisHandler::setRedis($cdnzone_num, json_encode($cdnzone_arr));
	}
	
	public function getCDNZones ($zonenum) {
		$cdnzone = json_decode(RedisHandler::getRedis($zonenum), true);
		if (count($cdnzone) > 0) {
			$return = $cdnzone["NEXT"];
			array_unshift($return, $zonenum);
		}
		else {
			$return = false;
		}
		return $return;
	}
	
	public function getCDNZoneGroup ($zonenum, $type) {
		$cdnzone = json_decode(RedisHandler::getRedis($zonenum), true);
		$return = (isset($cdnzone["TYPE"][$type]) ? $cdnzone["TYPE"][$type] : false);
		return $return;
	}
}