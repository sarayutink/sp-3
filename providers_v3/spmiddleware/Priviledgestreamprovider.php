<?
require_once 'Priviledgestreamprovider.req';

class Priviledgestreamprovider {
	public static function provide ($request, $response) {
		$body = $request->getBody();
		$GLOBALS['ctrl_name'] = "Priviledgestreamprovider";
		$GLOBALS['lbl_name'] = "lalaynya";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		
		## validate post json
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
			require_once($GLOBALS['src_dir'] ."/configures/priviledgestreamprovider/".$apconf.".php");
			$GLOBALS['bizconf'] = new $apconf();
			## check channel
			if ($GLOBALS['bizconf']->isValidChannelID()) {
				## check ccu control
				require_once($GLOBALS['src_dir'] ."/controllers/Ccucontrol.php");
				if (Ccucontrol::check() == 200) {
					## get streaming server via load balancer
					require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
					$server = Loadbalancecontrol::findServer();
					if (strrpos($server, "truevisions.tv")) {
						require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol.php");
						$action = Streamcontrol::createStreamPath();
						if(!is_null($action)) {
							$return = array('result_code' => 200, 'result' => "http://".$server.$action);
							Logger::writelog(array('result_code' => 200, 'result' => "http://".$server.$action));
						}
						else $return = array('result_code' => 430, 'result' => "Cannot_find_playlist.");
					}
					// elseif ($server == "406") $return = array('result_code' => 200, 'result' => "406");
					else $return = array('result_code' => 420, 'result' => "Cannot find streaming server.");
				}
				else $return = array('result_code' => 410, 'result' => "Your concurrent reaches limit.");
			}
			else $return = array('result_code' => 610, 'result' => "Cannot find channel to streaming.");
		}
		else $return = array('result_code' => 600, 'result' => "Invalid request.");
		
		
		return $return;
    }
}