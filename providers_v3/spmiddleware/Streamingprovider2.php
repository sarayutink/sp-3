<?
require_once 'Streamingprovider2.req';

class Streamingprovider2 {
	public static function provide ($request, $response) {
		#$body = $request->getBody();
		$body = file_get_contents("php://input");
		$GLOBALS['ctrl_name'] = "Streamingprovider2";
		$GLOBALS['lbl_name'] = "lalaynya";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		## validate post json
		$isBlacklist = Blacklist::isBlacklist();
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
			if (file_exists($GLOBALS['src_dir'] ."/configures/streamingprovider2/".$apconf.".php")) {
				require_once($GLOBALS['src_dir'] ."/configures/streamingprovider2/".$apconf.".php");
				$GLOBALS['bizconf'] = new $apconf();
				## get streaming server via load balancer
				require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
				$actionURL = $GLOBALS['bizconf']->getBalencer();
				$server = Loadbalancecontrol::findServer($actionURL, @$in_group);
				// require_once($GLOBALS['src_dir'] ."/controllers/Roundbalancecontrol.php");
				// $in_group = $GLOBALS['bizconf']->getGroupId();
				// $server = Roundbalancecontrol::findServer(11); 
				if (intval($server) == 0) {
					if (!preg_match("/^(wv|fp)$/", @$GLOBALS['json_object']->drm)) {
						require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol.php");
						$action = Streamcontrol::createStreamPath();
						if(!is_null($action)) {
							$protocol = "http";
							//if (!in_array($GLOBALS['json_object']->channelid, explode("|", "107|en107")) && in_array($GLOBALS['json_object']->appid, explode("|", "trueidv2|truetv")) && in_array($GLOBALS['json_object']->visitor, explode("|", "mobile")) && in_array($GLOBALS['json_object']->type, explode("|", "live"))) $protocol = "https";
							$return = array('result_code' => 200, 'result' => "$protocol://".$server.$action);
							Logger::writelog(array('result_code' => 200, 'result' => "$protocol://".$server.$action));
						}
						else $return = array('result_code' => 430, 'result' => "Cannot find playlist.");
					}
					else {
						require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol_dev.php");
						$action = Streamcontrol_dev::createStreamManifest();
						if(!is_null($action)) {
							$return = array('result_code' => 200, 'result_description' => "SUCCESS", 'streamurl' => "https://".$server.$action['manifest'], 'license' => $GLOBALS['bizconf']->bizconf->license->{$GLOBALS['json_object']->drm} ."?". $action['license']);
							// $return = array('result_code' => 200, 'result_description' => "SUCCESS", 'streamurl' => "https://".$server.$action['manifest']);
							Logger::writelog(array('result_code' => 200, 'result' => "https://".$server.$action['manifest'], 'license' => $action['license']));
						}
						else $return = array('result_code' => 430, 'result_description' => "Cannot find playlist.");
					}
				}
				// elseif ($server == "106") $return = array('result_code' => 200, 'result' => "406");
				else {
					$return = array('result_code' => 420, 'result' => "Cannot find streaming server.");
					Logger::writelog(array('result_code' => 429, 'result' => $server));
				}
			}
			else {
				$return = array('result_code' => 600, 'result' => "Invalid request.");
				Logger::writelog(array('result_code' => 620, 'result' => "INVALID_APPID"));
			}
		}
		elseif (!$isBlacklist) {
			Logger::writelog(array('result_code' => 640, 'result' => "Blacklisted."));
			$return = array('result_code' => 640, 'result' => "UID_BANNED");
		}
		else {
			$return = array('result_code' => 600, 'result' => "Invalid request.");
			Logger::writelog(array('result_code' => 640, 'result' => "INVALID_PARAMETERS"));
		}
		
		
		return $return;
    }
}
