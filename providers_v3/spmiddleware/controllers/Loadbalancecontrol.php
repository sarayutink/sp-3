<?
class Loadbalancecontrol {	
	public static function findServer($actionURL = "http://server_loadbalan:8080/lalaynya.php", $in_group = null) {
		require_once $GLOBALS['src_dir'] .'/libraries/Curlcstm.php';
		// $group = $GLOBALS['bizconf']->getGroupId();
		$group = is_null($in_group) ? self::getGroupId() : $in_group;
		// var_dump($group);
		if (!empty($group)) {
			// if ($group == 13) {
				// Logger::writelog(array('result_code' => 499, 'result' => 'MOCK_UP_STREAM_SERVER'));
				// return "tvsedge142.truevisions.tv";
			// }
			require_once $GLOBALS['src_dir'] .'/libraries/Demeter.php';
			$lb = new Demeter();
			$cdn = $lb->findCDN(intval($group));
			if (is_int(strpos($cdn, "cdn"))) return $cdn;
			return 423;
			// $lbUrl = "$actionURL?group=$group&uid={$GLOBALS['json_object']->uid}";
			// $result = Curlcstm::establishServer($lbUrl);
			// if ($result <> false) {
			// 	switch (trim($result)) {
			// 		case "006" :
			// 			Logger::writelog(array('result_code' => 460, 'result' => 'LOAD_BALAN_CACHE_FAIL'));
			// 			return $result;
			// 		break;
			// 		case "106" :
			// 		Logger::writelog(array('result_code' => 461, 'result' => 'STREAM_GROUP_'.$group.'_REACH_MAX'));
			// 		return $result;
			// 	break;
			// 	case "206" :
			// 		Logger::writelog(array('result_code' => 462, 'result' => 'NOT_FOUND_STREAM_SERVER_IN_GROUP_'.$group));
			// 		return $result;
			// 	break;
			// 	case "306" :
			// 		Logger::writelog(array('result_code' => 463, 'result' => 'NOT_FOUND_STREAM_RUNNING_IN_GROUP_'.$group));
			// 		return $result;
			// 	case "406" :
			// 		Logger::writelog(array('result_code' => 464, 'result' => 'LOAD_BALAN_CONFIG_FAIL'));
			// 		return $result;
			// 	break;
			// 	case "506" :
			// 		Logger::writelog(array('result_code' => 465, 'result' => 'STREAM_SERVER_'.$group.'_REACH_MAX'));
			// 			return $result;
			// 		break;
			// 		case "" :
			// 			Logger::writelog(array('result_code' => 424, 'result' => 'STREAM_SERVER_RESPOND_FAIL'));
			// 			return 424;
			// 		break;
			// 		default :
			// 			if (isset($GLOBALS['json_object']->charge) && $GLOBALS['json_object']->charge === false) $result = str_replace("tvs", "fch", $result);
			// 			return $result;
			// 		break;
			// 	}
			// }
			// else {
			// 	Logger::writelog(array('result_code' => 423, 'result' => 'LOAD_BALAN_NOT_RESPOND'));
			// 	return 423;
			// }
		}
		elseif ($group === 0) {
			Logger::writelog(array('result_code' => 422, 'result' => 'GROUP_IS_0'));
			return 422;
		} 
		else {
			Logger::writelog(array('result_code' => 421, 'result' => 'SERVER_GROUP_NOT_FOUND'));
			return 421;
		}
	}
	
	private static function getGroupId () {
		
		$conf_path = "/www/webapps/providers_v3_static_group.xml";
		$group_conf = simplexml_load_file($conf_path);

		## channelid group
		$except_ch_reg = array("004", "008", "024", "057", "068", "072", "073", "081", "113", "117", "173", "176", "221", "223", "ht001", "ht002", "ht003", "ht004", "ht005", "ht006", "ht007", "ht008", "ht009", "st002", "st003", "st004", "st005");
		$inter_ch_reg = array("en107", "107");
		$group = 0;
		if (!in_array($GLOBALS['json_object']->channelid, $except_ch_reg) && !in_array($GLOBALS['json_object']->channelid, $inter_ch_reg)) {		
			$group = $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->type};
		}
		if (in_array($GLOBALS['json_object']->channelid, $inter_ch_reg) && $GLOBALS['json_object']->appid != "stg") {
			$group = 6;
		}
		// if (in_array($GLOBALS['json_object']->channelid, explode("|", "c03|207|d05|da7|da0|135|034|d56|c09|159")) && $GLOBALS['json_object']->appid == "trueidv2" && $GLOBALS['json_object']->type == "live" && $GLOBALS['json_object']->appid != "stg") {
		// 	$group = 4;
		// }
		// if (in_array($GLOBALS['json_object']->channelid, explode("|", "d83|d43|143|152|094|d62|c12|d48|d33|d78")) && $GLOBALS['json_object']->appid == "trueidv2" && $GLOBALS['json_object']->type == "live" && $GLOBALS['json_object']->appid != "stg") {
		// 	$group = 5;
		// }
		if (in_array($GLOBALS['json_object']->channelid, $except_ch_reg) && !in_array($GLOBALS['json_object']->appid, array("stg", "antv1"))) {
			$group = $group_conf->truetv->{$GLOBALS['json_object']->type};
		}
		if (in_array($GLOBALS['json_object']->channelid, $except_ch_reg) && $GLOBALS['json_object']->appid == "stg") {
			$group = $group_conf->stg->{$GLOBALS['json_object']->type};
		}
		if (in_array($GLOBALS['json_object']->channelid, $except_ch_reg) && $GLOBALS['json_object']->appid == "antv1") {		
			$group = $group_conf->antv1->{$GLOBALS['json_object']->type};
		}
		// else {
		// 	$group = $group_conf->truetv->{$GLOBALS['json_object']->type};
		// }
		## visitor group
		if ($GLOBALS['json_object']->type == "live" && $GLOBALS['json_object']->visitor == "chromecast") $group = 1;
		## appid group
		// $group = ($GLOBALS['json_object']->channelid <> "ev01" ? $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->type} : 6);
		// $group = $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->type};
		if ($group == 0) $group = $group_conf->base->{$GLOBALS['json_object']->type};
		// echo (string)$group;
		return (string)$group;
		//in_array($GLOBALS['json_object']->appid, explode("|", "trueidv2|trueidtv"))
		
		/*
		## channelid group
		$except_ch_reg = explode("|", "024|057|081|068|008|117|176|221|173|072|073|113|223|004");
		$inter_ch_reg = explode("|", "en107|107");
		$group = 0;
		if (!in_array($GLOBALS['json_object']->channelid, $except_ch_reg) && !in_array($GLOBALS['json_object']->channelid, $inter_ch_reg)) {
			$group = $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->type};
		}
		if (in_array($GLOBALS['json_object']->channelid, $inter_ch_reg) && $GLOBALS['json_object']->appid != "stg") {
			$group = 6;
		}
		// if (in_array($GLOBALS['json_object']->channelid, explode("|", "c03|207|d05|da7|da0|135|034|d56|c09|159")) && $GLOBALS['json_object']->appid == "trueidv2" && $GLOBALS['json_object']->type == "live" && $GLOBALS['json_object']->appid != "stg") {
		// 	$group = 4;
		// }
		// if (in_array($GLOBALS['json_object']->channelid, explode("|", "d83|d43|143|152|094|d62|c12|d48|d33|d78")) && $GLOBALS['json_object']->appid == "trueidv2" && $GLOBALS['json_object']->type == "live" && $GLOBALS['json_object']->appid != "stg") {
		// 	$group = 5;
		// }
		if (in_array($GLOBALS['json_object']->channelid, $except_ch_reg) && $GLOBALS['json_object']->appid != "stg") {
			$group = $group_conf->truetv->{$GLOBALS['json_object']->type};
		}
		if (in_array($GLOBALS['json_object']->channelid, $except_ch_reg) && $GLOBALS['json_object']->appid == "stg") {
			$group = $group_conf->stg->{$GLOBALS['json_object']->type};
		}
		// else {
		// 	$group = $group_conf->truetv->{$GLOBALS['json_object']->type};
		// }
		## visitor group
		if ($GLOBALS['json_object']->type == "live" && $GLOBALS['json_object']->visitor == "chromecast") $group = 1;
		## appid group
		// $group = ($GLOBALS['json_object']->channelid <> "ev01" ? $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->type} : 6);
		// $group = $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->type};
		if ($group == 0) $group = $group_conf->base->{$GLOBALS['json_object']->type};
		// echo (string)$group;
		return (string)$group;
		//in_array($GLOBALS['json_object']->appid, explode("|", "trueidv2|trueidtv")) */
		
	}
}
?>