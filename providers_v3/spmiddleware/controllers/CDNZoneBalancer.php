<?
class CDNZoneBalancer {	
	public static function findServerByIP ($actionURL = "http://172.22.160.29:8080/lalaynya.php") {
		require_once $GLOBALS['src_dir'] .'/libraries/Curlcstm.php';
		$zones = self::getZonesByIP();
		foreach ($zones as $zone) {
			$group = self::getGroupidByIP($zone);
			$lbUrl = "$actionURL?group=$group&uid={$GLOBALS['json_object']->uid}";
			$result = Curlcstm::establishServer($lbUrl);
			if ($result <> false) if (strpos($result, "edge") <> false) break;
		}
		if (!empty($group)) {
			if ($group == 13) {
				Logger::writelog(array('result_code' => 499, 'result' => 'MOCK_UP_STREAM_SERVER'));
				return "tvsedge142.truevisions.tv";
			}
			$lbUrl = "$actionURL?group=$group&uid={$GLOBALS['json_object']->uid}";
			$result = Curlcstm::establishServer($lbUrl);
			if ($result <> false) {
				switch ($result) {
					case "006" :
						Logger::writelog(array('result_code' => 460, 'result' => 'LOAD_BALAN_CACHE_FAIL'));
						return $result;
					break;
					case "106" :
						Logger::writelog(array('result_code' => 461, 'result' => 'STREAM_GROUP_REACH_MAX'));
						return $result;
					break;
					case "206" :
						Logger::writelog(array('result_code' => 462, 'result' => 'NOT_FOUND_STREAM_SERVER_IN_GROUP'));
						return $result;
					break;
					case "306" :
						Logger::writelog(array('result_code' => 463, 'result' => 'NOT_FOUND_STREAM_RUNNING_IN_GROUP'));
						return $result;
					case "406" :
						Logger::writelog(array('result_code' => 464, 'result' => 'LOAD_BALAN_CONFIG_FAIL'));
						return $result;
					break;
					case "506" :
						Logger::writelog(array('result_code' => 465, 'result' => 'STREAM_SERVER_REACH_MAX'));
						return $result;
					break;
					case "" :
						Logger::writelog(array('result_code' => 424, 'result' => 'STREAM_SERVER_RESPOND_FAIL'));
						return 424;
					break;
					default :
						if (isset($GLOBALS['json_object']->charge) && $GLOBALS['json_object']->charge === false) $result = str_replace("tvs", "fch", $result);
						return $result;
					break;
				}
			}
			else {
				Logger::writelog(array('result_code' => 423, 'result' => 'LOAD_BALAN_NOT_RESPOND'));
				return 423;
			}
		}
		elseif ($group === 0) {
			Logger::writelog(array('result_code' => 422, 'result' => 'GROUP_IS_0'));
			return 422;
		} 
		else {
			$group = self::getGroupidByIP(1);
			$lbUrl = "$actionURL?group=$group&uid={$GLOBALS['json_object']->uid}";
			$result = Curlcstm::establishServer($lbUrl);
			var_dump($group);
			if ($result <> false) {
				switch ($result) {
					case "006" :
						Logger::writelog(array('result_code' => 460, 'result' => 'LOAD_BALAN_CACHE_FAIL'));
						return $result;
					break;
					case "106" :
						Logger::writelog(array('result_code' => 461, 'result' => 'STREAM_GROUP_REACH_MAX'));
						return $result;
					break;
					case "206" :
						Logger::writelog(array('result_code' => 462, 'result' => 'NOT_FOUND_STREAM_SERVER_IN_GROUP'));
						return $result;
					break;
					case "306" :
						Logger::writelog(array('result_code' => 463, 'result' => 'NOT_FOUND_STREAM_RUNNING_IN_GROUP'));
						return $result;
					case "406" :
						Logger::writelog(array('result_code' => 464, 'result' => 'LOAD_BALAN_CONFIG_FAIL'));
						return $result;
					break;
					case "506" :
						Logger::writelog(array('result_code' => 465, 'result' => 'STREAM_SERVER_REACH_MAX'));
						return $result;
					break;
					case "" :
						Logger::writelog(array('result_code' => 424, 'result' => 'STREAM_SERVER_RESPOND_FAIL'));
						return 424;
					break;
					default :
						if (isset($GLOBALS['json_object']->charge) && $GLOBALS['json_object']->charge === false) $result = str_replace("tvs", "fch", $result);
						return $result;
					break;
				}
			}
			else {
				Logger::writelog(array('result_code' => 423, 'result' => 'LOAD_BALAN_NOT_RESPOND'));
				return 423;
			}
		}
	}
	
	private static function getZonesByIP () {
		require_once($GLOBALS['src_dir'] .'/libraries/cdnrouter/PrefixLoader.php');
		require_once($GLOBALS['src_dir'] .'/libraries/cdnrouter/CDNZone.php');
		$tree = json_decode(RedisHandler::getRedis("PrefixLoader"), true);
		$search = new PrefixSearch();
		$search->loadPrefixes($tree);
		$cdnzone_num = $search->searchThroughPrefix($GLOBALS['json_object']->csip);
		$cdnzone = new CDNZone();
		$zones = $cdnzone->getCDNZones($cdnzone_num);
		return $zones;
	}
	
	private static function getGroupidByIP ($cdnzone_num) {
		require_once($GLOBALS['src_dir'] .'/libraries/cdnrouter/CDNZone.php');
		if (strpos($GLOBALS['json_object']->type, "vod") <> false || strpos($GLOBALS['json_object']->type, "mov") <> false) $type = "VOD";
		elseif (strpos($GLOBALS['json_object']->type, "clip") <> false) $type = "CLIP";
		else $type = strtoupper($GLOBALS['json_object']->type);
		$cdnzone = new CDNZone();
		$gruop = $cdnzone->getCDNZoneGroup($cdnzone_num, strtoupper($type));
		return $gruop;
	}
}
?>