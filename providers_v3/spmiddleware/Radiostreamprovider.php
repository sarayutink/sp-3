<?
require_once 'Radiostreamprovider.req';

class Radiostreamprovider {
	public static function provide ($request, $response) {
		$body = $request->getBody();
		$GLOBALS['ctrl_name'] = "Radiostreamprovider";
		$GLOBALS['lbl_name'] = "lalaynya";
		$GLOBALS['json_object'] = json_decode($body);
		// unset($body);
		
		## validate post json
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
			require_once($GLOBALS['src_dir'] ."/configures/radiostreamprovider/".$apconf.".php");
			$GLOBALS['bizconf'] = new $apconf();
			## check ccu control
			// require_once($GLOBALS['src_dir'] ."/controllers/Ccucontrol.php");
			// if (Ccucontrol::check()) {
			if (true) {
				## get streaming server via load balancer
				require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
				$server = Loadbalancecontrol::findServer("http://server_loadbalan:8080/lalaynya.php", 22);
				// $server = Loadbalancecontrol::findServer("http://172.22.160.31:8080/agentB.php", 36);
				// $server = Loadbalancecontrol::findServer("http://172.22.222.21/tvslb", 36);
				// if (strrpos($server, "truelife.com")) {
				if (true) {
					// $server = ($GLOBALS['json_object']->visitor == "mobile") ? "61.90.170.90:554/" : "61.90.170.90/";
					// $server .= ;
					require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol.php");
					$action = Streamcontrol::createStreamPath();
					if(!is_null($action)) {
						$protocol = ($GLOBALS['json_object']->visitor == "wap") ? "rtsp://" : "http://";
						// $server = ($GLOBALS['json_object']->visitor == "wap") ? str_replace(":80/", "", $server) .":554/" : $server;
						$server .= ($GLOBALS['json_object']->visitor == "wap") ? ":554/" : "/";
						// $protocol = "http://";
						$return = array('result_code' => 200, 'result' => $protocol.$server.$action);
						Logger::writelog(array('result_code' => 200, 'result' => $protocol.$server.$action));
					}
					else $return = array('result_code' => 430, 'result' => "Cannot find playlist.");
				}
				// elseif ($server == "406") $return = array('result_code' => 200, 'result' => "406");
				else $return = array('result_code' => 420, 'result' => "Cannot find streaming server.");
			}
			else $return = array('result_code' => 410, 'result' => "Your concurrent reaches limit.");
		}
		else $return = array('result_code' => 600, 'result' => "Invalid request.");
		
		return $return;
    }
}