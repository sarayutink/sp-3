<?
include_once( dirname(__DIR__). '/Privstreamconfig.php');

class Trueid2config extends Privstreamconfig {
	public function __construct () {
		parent::__construct();
	}
	
	public function getGroupId () {
		return 6;
	}
	
	public function generatePlaylist () {
		// echo $object->programstate;
		$visitor = $GLOBALS['json_object']->visitor;
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'" and @device="'. strtolower($visitor) .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);
		if (count($ret_arr) > 0) {
			// var_dump($object);
			$smil = str_replace("yyyymmdd", date("Ymd", (int)$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
			return "/".(string)$ret_arr[0]->appinst."/". $smil ."/playlist.m3u8";
		}
		else return null;
	}
	
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$token_arr = array('oqxg55CptqALr0','aFEacfYcpOtkGi','famnCWFwjvlksk','5M1L5wcBblA0IX','oP9Qlqas8YJlvp','9UxURr9saLEd5F');
		$channelid = ($GLOBALS['json_object']->channelid != '021') ? $GLOBALS['json_object']->channelid : '135';
		$ch_list = "o021;o022;179";
		$encrypt = ((strpos($ch_list, $GLOBALS['json_object']->channelid) !== false)) ? Opensslcryption::encryptbypass() : Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $channelid ."|". $GLOBALS['json_object']->uid ."|". $GLOBALS['bizconf']->bizconf->user);
		
		if ($GLOBALS['json_object']->type == "timeshift") @$querystring = "dvr=&";
		elseif ($GLOBALS['json_object']->type == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']->stime, "Y-m-d-H:i:s")}&wowzadvrplaylistduration={$GLOBALS['json_object']->duration}&";
		@$querystring .= "appid={$GLOBALS['json_object']->appid}&visitor={$GLOBALS['json_object']->visitor}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
}
?>