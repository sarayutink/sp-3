<?
class Vodconfig {
	var $bizunit;
	
	public function __construct () {
		$this->bizconf = $this->loadBizConf($GLOBALS['json_object']->appid.".conf", $GLOBALS['src_dir'] ."/assets/". strtolower($GLOBALS['ctrl_name']) ."/");
	}
	
	private function loadBizConf ($appname, $path = APPPATH) {
		if (file_exists($path.$appname)) {
			/** load config in XML */
			try {
				return simplexml_load_file($path.$appname);
			}
			catch(Exception $e) {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public function verifyApplicationCCU ($ccucount = false) {
		if ($this->bizconf->application->ccu == 'unlimit') return true;
		else return $ccucount < $this->bizconf->application->ccu;
	}
	
	public function verifyUserCCU ($ccucount = false) {
		if ($this->bizconf->user->ccu == 'unlimit') return true;
		else return $ccucount < $this->bizconf->user->ccu;
	}
	
	public function generatePlaylist () {
		return "/". $this->bizconf->streammapping->AppInst ."/". $GLOBALS['json_object']->streamname. "/playlits.m3u8";
	}
	
	public function getBalencer () {
		return "http://server_loadbalan:8080/lalaynya.php";
	}
	
	public function generateManifest () {
		switch (@$GLOBALS['json_object']->drm) {
			case "wv":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Wvconfig.php";
				return Wvconfig::generateManifest();
			break;
			case "fp":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Fpconfig.php";
				return Fpconfig::generateManifest();
			break;
			default:
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Aesconfig.php";
				return Aesconfig::generateManifest();
			break;
		}
	}
	
	public function getGroupId () {
		$drm = (@$GLOBALS['json_object']->drm == "wv" || @$GLOBALS['json_object']->drm == "fp") ? $GLOBALS['json_object']->drm : "aes";
		return $this->bizconf->$drm->group;
		// $group_arr = array('sportclip_truetv', 'sportclip_trueidv2', 'skyclipv2');
		// return (in_array($GLOBALS['json_object']->appid, $group_arr)) ? 103 : 105;
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
	}
	
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		//require_once $GLOBALS['src_dir'] .'/libraries/ArsOpensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". explode("_", $GLOBALS['json_object']->streamname)[0] ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		//$encrypt = Arsopensslcryption::encrypt($str2decrypt);
		// $encrypt = Opensslcryption::encryptbypass();
		$querystring = "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&sessionid={$GLOBALS['json_object']->sessionid}&mpass={$encrypt}&visitor={$GLOBALS['json_object']->visitor}";
		
		return $querystring;
	}
	
	public function getLicense () {
		switch (@$GLOBALS['json_object']->drm) {
			case "wv":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Wvconfig.php";
				return Wvconfig::generateLicense();
			break;
			case "fp":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Fpconfig.php";
				return Fpconfig::generateLicense();
			break;
			default:
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Aesconfig.php";
				return Aesconfig::generateLicense();
			break;
		}
	}
}