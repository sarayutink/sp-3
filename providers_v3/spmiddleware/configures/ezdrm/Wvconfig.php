<?
class Wvconfig {
	public static function generateManifest () {
		switch ($GLOBALS['ctrl_name']) {
			case 'Streamingprovider2' :
				$manifest = self::generateLiveSmil();
			break;
			default :
				$manifest = self::generateVodSmil();
			break;
		}
		return $manifest;
	}
	
	public static function generateLicense () {
		switch ($GLOBALS['ctrl_name']) {
			case 'Streamingprovider2' :
				$qstring = self::generateLiveQString();
			break;
			default :
				$qstring = self::generateVodQString();
			break;
		}
		return $qstring;
	}
	
	public static function generateByPassLicense () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$encrypt = Opensslcryption::encryptbypass();
		$querystring = "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
	
	private static function generateLiveSmil () {
		// $visitor = "mobile";
		// $query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'"  and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		// $ret_arr = $GLOBALS['bizconf']->bizctrl->xpath($query);
		// if (count($ret_arr) > 0) {
		// 	require_once $GLOBALS['src_dir'] .'/configures/Blackoutconfig.php';
		// 	// var_dump($object);
		// 	$smil = str_replace("yyyymmdd", date("Ymd", (int)$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
		// 	if ($GLOBALS['json_object']->type == "live" && Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid)) $smil = "bk_m_auto.smil";
			
		// 	return isset($GLOBALS['bizconf']->bizconf->wv->appinst) ? "/". $GLOBALS['bizconf']->bizconf->wv->appinst ."/". $smil ."/manifest.mpd?appid=". $GLOBALS['json_object']->appid ."&uid=". $GLOBALS['json_object']->uid ."&visitor=". $GLOBALS['json_object']->visitor : null;
		// }
		// else return null;

		$query = implode(";", array($GLOBALS['json_object']->appid, $GLOBALS['json_object']->langid, $GLOBALS['json_object']->type, $GLOBALS['json_object']->visitor, $GLOBALS['json_object']->streamlvl, "aes", $GLOBALS['json_object']->channelid));
		$default = implode(";", array("htv", $GLOBALS['json_object']->langid, $GLOBALS['json_object']->type, $visitor, $GLOBALS['json_object']->streamlvl, "aes", $GLOBALS['json_object']->channelid));
		$return = $GLOBALS['redis']->getRedis($query, 1, array('host' => '10.18.19.98', 'port' => 6380));
		if ($return !== false) {
			$return = str_replace("yyyymmdd", date("Ymd", (int)@$GLOBALS['json_object']->stime), $return);
			// file_put_contents("/www/logs/generatePlaylist.sp2.log", $default ." >> ".$return."\n",FILE_APPEND);
			return "/".$return."/manifest.mpd?appid=". $GLOBALS['json_object']->appid ."&uid=". $GLOBALS['json_object']->uid ."&visitor=". $GLOBALS['json_object']->visitor;
		}
		else {
			$return = $GLOBALS['redis']->getRedis($default, 1, array('host' => '10.18.19.98', 'port' => 6380));
			if ($return !== false) {
				$return = str_replace("yyyymmdd", date("Ymd", (int)@$GLOBALS['json_object']->stime), $return);
				// file_put_contents("/www/logs/generatePlaylist.sp2.log", $default ." >> ".$return."\n",FILE_APPEND);
				return "/".$return."/manifest.mpd?appid=". $GLOBALS['json_object']->appid ."&uid=". $GLOBALS['json_object']->uid ."&visitor=". $GLOBALS['json_object']->visitor;
			}
			else return null;
		}
	}
	
	private static function generateVodSmil () {
		$streamname = "";
		// if (is_numeric($GLOBALS['json_object']->streamname)) {
		if (strpos($GLOBALS['json_object']->appid, "clip") === false) {
			// $streamname = ($GLOBALS['json_object']->visitor == "stb") ? "_stb_auto.smil" : "_m_auto.smil";
			$streamname = "_m_auto.smil";
			if (is_numeric($GLOBALS['json_object']->streamname)) $streamname = str_pad($GLOBALS['json_object']->streamname, 12, "0", STR_PAD_LEFT) . $streamname;
			else $streamname = $GLOBALS['json_object']->streamname . $streamname;
		}
		else {
			$streamname = $GLOBALS['json_object']->streamname;
		}
		
		return isset($GLOBALS['bizconf']->bizconf->wv->appinst) ? "/". $GLOBALS['bizconf']->bizconf->wv->appinst ."/". $streamname ."/manifest.mpd?appid=". $GLOBALS['json_object']->appid ."&uid=". $GLOBALS['json_object']->uid ."&visitor=". $GLOBALS['json_object']->visitor : null;
	}
	
	private static function generateLiveQString () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $GLOBALS['json_object']->channelid ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		$querystring = isset($GLOBALS['bizconf']->bizconf->drm->{$GLOBALS['json_object']->drm}) ? "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&sessionid={$GLOBALS['json_object']->sessionid}&mpass={$encrypt}": null;
		
		return $querystring;
	}
	
	private static function generateVodQString () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". explode("_", $GLOBALS['json_object']->streamname)[0] ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		$querystring = isset($GLOBALS['bizconf']->bizconf->drm->{$GLOBALS['json_object']->drm}) ? "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&sessionid={$GLOBALS['json_object']->sessionid}&mpass={$encrypt}": null;
		
		return $querystring;
	}
}