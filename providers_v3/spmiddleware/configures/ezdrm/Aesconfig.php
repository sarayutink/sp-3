<?
class Aesconfig {
	public static function generateManifest () {
		$streamname = "";
		// if (is_numeric($GLOBALS['json_object']->streamname)) {
		if (strpos($GLOBALS['json_object']->appid, "clip") === false) {
			// $streamname = ($GLOBALS['json_object']->visitor == "stb") ? "_stb_auto.smil" : "_m_auto.smil";
			$streamname = "_m_auto.smil";
			if (is_numeric($GLOBALS['json_object']->streamname)) $streamname = str_pad($GLOBALS['json_object']->streamname, 12, "0", STR_PAD_LEFT) . $streamname;
			else $streamname = $GLOBALS['json_object']->streamname . $streamname;
		}
		else {
			$streamname = $GLOBALS['json_object']->streamname;
		}
		
		return isset($GLOBALS['bizconf']->bizconf->aes->appinst) ? "/". $GLOBALS['bizconf']->bizconf->aes->appinst ."/". $streamname ."/playlist.m3u8?appid=". $GLOBALS['json_object']->appid ."&uid=". $GLOBALS['json_object']->uid ."&visitor=". $GLOBALS['json_object']->visitor : null;
	}
	
	public static function generateLicense () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". explode("_", $GLOBALS['json_object']->streamname)[0] ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		$querystring = "&mpass={$encrypt}";
		
		return $querystring;
	}
	
	public static function generateBypassLicense () {
		$encrypt = Opensslcryption::encryptbypass();
		$querystring = "?appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
}