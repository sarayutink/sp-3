<?
class Privstreamconfig {
	var $bizctrl;
	var $bizconf;
	var $appdb;
	var $confdb = 0;
	var $chdb = 1;
	
	public function __construct () {
		$this->bizctrl = $this->loadRedisXML();
		$this->bizconf = $this->loadRedisConfig();
		$this->appdb = $this->getAppDB();
	}
	
	// private function loadBizConf ($appname, $path = APPPATH) {
		// if (file_exists($path.$appname)) {
			// /** load config in XML */
			// try {
				// return simplexml_load_file($path.$appname);
			// }
			// catch(Exception $e) {
				// return false;
			// }
		// }
		// else {
			// return false;
		// }
	// }
	
	private function loadRedisXML () {
		$xml_str = "<?xml version='1.0' encoding='UTF-8'?><configure>";
		$xml_str .= '<streammapping>'. $GLOBALS['redis']->getRedis($GLOBALS['json_object']->channelid, 1) .'</streammapping>';
		$xml_str .= '</configure>';
		
		return simplexml_load_string($xml_str);
	}
	
	private function loadRedisConfig () {
		$json_str = $GLOBALS['redis']->getRedis($GLOBALS['json_object']->appid .":". $GLOBALS['json_object']->priviledge .":config", 0);
		
		return json_decode($json_str);
	}
	
	private function getAppDB () {
		$serial = $GLOBALS['redis']->getRedis($GLOBALS['json_object']->appid .":db", 0);
		
		return unserialize($serial);
	}
	
	private function loadLocalXML ($filepath) {
		if (file_exists($filepath)) {
			/** load config in XML */
			try {
				return simplexml_load_file($filepath);
			}
			catch(Exception $e) {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public function isValidChannelID () {
		$type = $GLOBALS['json_object']->type;
		return in_array($GLOBALS['json_object']->channelid, $this->bizconf->$type);
	}
	
	public function generatePlaylist () {
		// echo $object->programstate;
		$visitor = ($GLOBALS['json_object']->visitor == "web" ? "mobile" : $GLOBALS['json_object']->visitor);
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'" and @device="'. strtolower($visitor) .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);
		if (count($ret_arr) > 0) {
			// var_dump($object);
			$smil = str_replace("yyyymmdd", date("Ymd", (int)$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
			return "/".(string)$ret_arr[0]->appinst."/". $smil ."/playlist.m3u8";
		}
		else return null;
	}
	
	public function getGroupId () {
		$visitor = ($GLOBALS['json_object']->visitor == "web" ? "mobile" : $GLOBALS['json_object']->visitor);
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. $GLOBALS['json_object']->streamlvl .'" and @device="'. $visitor .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);
		// var_dump($query);
		if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
		else return null;
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
	}
	
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		//require_once $GLOBALS['src_dir'] .'/libraries/ArsOpensslcryption.php';
		$token_arr = array('oqxg55CptqALr0','aFEacfYcpOtkGi','famnCWFwjvlksk','5M1L5wcBblA0IX','oP9Qlqas8YJlvp','9UxURr9saLEd5F');
		$channelid = ($GLOBALS['json_object']->channelid != '021') ? $GLOBALS['json_object']->channelid : '135';
		// $GLOBALS['json_object']->uid = (empty($GLOBALS['json_object']->uid) ? $this->generateRandomString() : $GLOBALS['json_object']->uid);
		// $GLOBALS['json_object']->sessionid = (empty($GLOBALS['json_object']->sessionid) ? $this->generateRandomString() : $GLOBALS['json_object']->sessionid);
		// $GLOBALS['json_object']->csip = (empty($GLOBALS['json_object']->csip) ? $this->generateRandomString() : $GLOBALS['json_object']->csip);
		// $ch_list = "d78";
		$ch_list = "o021;o022;179";
		// $ch_list = "c03;o064;o059;082;o019;114;da1;o058;o016;110;o015;o032;o071;109;o030;108;o066;o065;o025;111;o062;o060;o029;086;d23;d54;o045;127;218;o020;151;o014;154;o017;159;148;o026;o031;o018;o023;da0;d81;o042;107;d78;c12;da7;d11;o053;d13;o027;o043;d76;d56";
		$encrypt = ((strpos($ch_list, $GLOBALS['json_object']->channelid) !== false)) ? Opensslcryption::encryptbypass() : Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $channelid ."|". $GLOBALS['json_object']->uid ."|". $GLOBALS['bizconf']->bizconf->user);
		//$encrypt = ((strpos($ch_list, $GLOBALS['json_object']->channelid) !== false)) ? Arspensslcryption::encryptbypass() : Arspensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $channelid ."|". $GLOBALS['json_object']->uid ."|". $GLOBALS['bizconf']->bizconf->user);
		// $encrypt = Opensslcryption::encrypt($token_arr[rand(0,count($token_arr)-1)]);
		// $encrypt = Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $channelid ."|". $GLOBALS['json_object']->uid ."|". $this->bizctrl->user->ccu);
		if ($GLOBALS['json_object']->type == "timeshift") @$querystring = "dvr=&";
		elseif ($GLOBALS['json_object']->type == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']->stime, "Y-m-d-H:i:s")}&wowzadvrplaylistduration={$GLOBALS['json_object']->duration}&";
		@$querystring .= "appid={$GLOBALS['json_object']->appid}&visitor={$GLOBALS['json_object']->visitor}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
	
	private function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	} 
}