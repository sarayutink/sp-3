<?
class Eventconfig {
	public $bizconf;
	
	public function __construct () {
		$this->bizconf = $this->loadRedisXML();
	}
	
	private function loadRedisXML () {
		require_once __DIR__ ."/Configdistributor.php";
		$conf_dist = new Configdistributor();
		$ctrl_name = strtolower($GLOBALS['ctrl_name']);
		$type = (strtolower($GLOBALS['json_object']->programstate) == "timeshift" && $ctrl_name == "eventstreamprovider" ? "live" : $GLOBALS['json_object']->programstate);
		// var_dump($conf_dist->distributeConfig($ctrl_name, strtolower($GLOBALS['json_object']->programid), $type));
		// return true;
		return $conf_dist->distributeConfig($ctrl_name, strtolower($GLOBALS['json_object']->programid), $type);
	}
	
	public function verifyApplicationCCU ($ccucount = false) {
		if ($this->bizconf->application->ccu == 'unlimit') return true;
		else return $ccucount < $this->bizconf->application->ccu;
	}
	
	public function verifyUserCCU ($ccucount = false) {
		if ($this->bizconf->user->ccu == 'unlimit') return true;
		else return $ccucount < $this->bizconf->user->ccu;
	}
	
	public function generatePlaylist () {
		$object = $GLOBALS['json_object'];
		switch (strtolower($object->programstate)) {
			case "live" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. strtolower($object->streamlvl) .'" and @device="'. strtolower($object->visitor) .'" and @type="'. strtolower($object->programstate).'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return "/".(string)$ret_arr[0]->appinst."/smil:".(string)$ret_arr[0]->smil[0]."/playlist.m3u8";
				else return null;
			break;
			case "timeshift" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="live"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return "/".(string)$ret_arr[0]->appinst."/smil:".(string)$ret_arr[0]->smil[0]."/playlist.m3u8";
				else return null;
			break;
			case "catchup" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. strtolower($object->streamlvl) .'" and @device="'. strtolower($object->visitor) .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) {
					$smil = str_replace("yyyymmdd", date("Ymd", (string)$ret_arr[0]->stime), (string)$ret_arr[0]->smil[0]);
					return "/".(string)$ret_arr[0]->appinst."/". $smil ."/playlist.m3u8";
				}
				else return null;
			break;
			case "vod" :
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) {
					$path  = explode("/", parse_url((string)$ret_arr[0]->override_url)['path']);
					$path[1] = "vodevent";
					// var_dump($path);
					return implode("/", $path);
				} 
				else return null;
			break;
			default :
				$query = '//program[@id="'. ($object->programid).'"]/state[@id="'. $object->programstate .'"]/streamprofile/language[@id="'.$object->langid.'"]/profile[@streamlevel="'.$object->streamlvl.'" and @device="'.$object->visitor.'" and @type="'. strtolower($object->programstate).'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return "/".(string)$ret_arr[0]->appinst."/smil:".(string)$ret_arr[0]->smil[0]."/playlist.m3u8";
				else return null;
			break;
		}
		// if ($object->type == "live") return "/".$ret_arr->live->appinst."/".$ret_arr->live->smil."/playlist.m3u8";
		// else if ($object->type == "timeshift") return "/".$ret_arr->timeshift->appinst."/".$ret_arr->timeshift->smil."/playlist.m3u8";
		// else return "/".$ret_arr->catchup->appinst."/".$ret_arr->catchup->smil."{$this->changeDateFormat($object->stime, "Ymd")}/playlist.m3u8";
	}
	
	public function getGroupId () {
		$object = $GLOBALS['json_object'];
		switch (strtolower($object->programstate)) {
			case "live" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
				else return null;
			break;
			case "timeshift" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="live"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
				else return null;
			break;
			case "catchup" :	
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'.strtolower($object->programstate).'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
				else return null;
			break;
			case "vod" :
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) {
					$host  = parse_url((string)$ret_arr[0]->override_url)['host'];
					preg_match_all('/\d+/', $host, $matches);
					// var_dump($matches[0][0]);
					// return 0;
					return $matches[0][0];
				}
				else return null;
			break;
			default :
				$query = '//program[@id="'. strtolower($object->programid) .'"]/state[@id="'. strtolower($object->programstate) .'"]/streamprofile/language[@id="'. strtolower($object->langid) .'"]/profile[@streamlevel="'. $object->streamlvl .'" and @device="'. $object->visitor .'" and @type="'. strtolower($object->programstate) .'"]';
				$ret_arr = $this->bizconf->xpath($query);
				if (count($ret_arr) > 0) return "/".(string)$ret_arr[0]->appinst."/smil:".(string)$ret_arr[0]->smil[0]."/playlist.m3u8";
				else return null;
			break;
		}
		
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
	}
	
	public function getrsaqstring () {
		//require_once $GLOBALS['src_dir'] .'/libraries/ArsOpensslcryption.php';
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		// $token_arr = array('oqxg55CptqALr0','aFEacfYcpOtkGi','famnCWFwjvlksk','5M1L5wcBblA0IX','oP9Qlqas8YJlvp','9UxURr9saLEd5F');
		// $ch_list = "c03;o064;o059;082;o019;114;da1;o058;o016;110;o015;o032;o071;109;o030;108;o066;o065;o025;111;o062;o060;o029;086;d23;d54;o045;127;218;o020;151;o014;154;o017;159;148;o026;o031;o018;o023;da0;d81;o042;107;d78;c12;da7;d11;o053;d13;o027;o043;d76;d56";
		// $encrypt = (!(strpos($ch_list, $GLOBALS['json_object']->channelid) !== false && $GLOBALS['json_object']->type == 'live')) ? Rsacryption::encrypt(time()) : Rsacryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $GLOBALS['json_object']->channelid ."|". $GLOBALS['json_object']->uid ."|". $GLOBALS['bizconf']->bizctrl->user->ccu);
		// $encrypt = Rsacryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $GLOBALS['json_object']->channelid ."|". $GLOBALS['json_object']->uid ."|". $this->bizctrl->user->ccu);
		$encrypt = Opensslcryption::encrypt(time());
		//$encrypt = Arsopensslcryption::encrypt(time());
		if ($GLOBALS['json_object']->type == "timeshift") @$querystring = "dvr=&";
		elseif ($GLOBALS['json_object']->type == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']->stime, "Y-m-d-H:i:s")}&wowzadvrplaylistduration={$GLOBALS['json_object']->duration}&";
		@$querystring .= "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
}