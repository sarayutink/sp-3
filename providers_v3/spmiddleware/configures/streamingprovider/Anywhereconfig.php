<?
include_once(dirname(__DIR__). '/Streamconfig.php');
class Anywhereconfig extends Streamconfig {
	public function __construct () {
		parent::__construct();
	}
	
	public function getGroupId () {
		$conf_path = "/www/webapps/providers_v3_static_group.xml";
		$group_conf = simplexml_load_file($conf_path);
		return $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->type};
	}
}
?>