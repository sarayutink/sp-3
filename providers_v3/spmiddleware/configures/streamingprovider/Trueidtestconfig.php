<?
include_once( dirname(__DIR__). '/Streamconfig.php');
class Trueidtestconfig extends Streamconfig {	
	public function __construct () {
		parent::__construct();
	}
	
	public function generatePlaylist () {
		// echo $object->programstate;
		$visitor = "";
		if ($GLOBALS['json_object']->visitor == "web" && $GLOBALS['json_object']->channelid == "202" ) $visitor = $GLOBALS['json_object']->visitor;
		else $visitor = ($GLOBALS['json_object']->visitor == "web" ? "mobile" : $GLOBALS['json_object']->visitor);
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'" and @device="'. strtolower($visitor) .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);
		if (count($ret_arr) > 0) {
			// var_dump($object);
			if ($this->isBlackout() == false) $smil = str_replace("yyyymmdd", date("Ymd", (int)$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
			else $smil = "bk_m_auto.smil";
			return "/".(string)$ret_arr[0]->appinst."/". $smil ."/playlist.m3u8";
		}
		else return null;
	}
	
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$token_arr = array('oqxg55CptqALr0','aFEacfYcpOtkGi','famnCWFwjvlksk','5M1L5wcBblA0IX','oP9Qlqas8YJlvp','9UxURr9saLEd5F');
		$channelid = ($GLOBALS['json_object']->channelid != '021') ? $GLOBALS['json_object']->channelid : '135';
		if ($this->isBlackout()) $channelid = "test";
		$ch_list = "o021;o022;179";
		$encrypt = ((strpos($ch_list, $GLOBALS['json_object']->channelid) !== false)) ? Opensslcryption::encryptbypass() : Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $channelid ."|". $GLOBALS['json_object']->uid ."|". $GLOBALS['bizconf']->bizconf->user);
		if ($GLOBALS['json_object']->type == "timeshift") @$querystring = "dvr=&";
		elseif ($GLOBALS['json_object']->type == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']->stime, "Y-m-d-H:i:s")}&wowzadvrplaylistduration={$GLOBALS['json_object']->duration}&";
		@$querystring .= "appid={$GLOBALS['json_object']->appid}&visitor={$GLOBALS['json_object']->visitor}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
	
	private function isBlackout () {
		$keys = $GLOBALS['redis']->getRedis("*;". $GLOBALS['json_object']->appid .";". $GLOBALS['json_object']->channelid .";*", 3);
		if (count($keys) > 0) {
			foreach ($keys as $key) {
				$cols = explode(";", $key);
				$now = time();
				if ($cols[1] <= $now && $now <= $cols[0]) return true;
			}
		}
		else return false;
		// foreach ($keys as $key) {
			// $key_arr = explode(";", $key);
			// if ($key_arr[0] > time() && $key_arr[1] < time()) {
				// $channels = $GLOBALS['redis']->getRedis($key, 3);
				// if (in_array($GLOBALS['json_object']->channelid, explode(";", $channels))) return true;
			// }
		// }
		// return false;
	}
	
	// public function getGroupId () {
		// $visitor = ($GLOBALS['json_object']->visitor == "web" ? "mobile" : $GLOBALS['json_object']->visitor);
		// $query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. $GLOBALS['json_object']->streamlvl .'" and @device="'. $visitor .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		// $ret_arr = $this->bizctrl->xpath($query);
		// var_dump($query);
		// if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
		// else return null;
	// }
}
?>