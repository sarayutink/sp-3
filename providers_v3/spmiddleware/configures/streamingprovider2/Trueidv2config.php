<?
include_once( dirname(__DIR__). '/Streamconfig2.php');
class Trueidv2config extends Streamconfig2 {
	public function __construct () {
		parent::__construct();
	}
	
	// public function getGroupId () {
		// $conf_path = "/www/webapps/providers_v3_static_ch2group.xml";
		// $group_conf = simplexml_load_file($conf_path);
		// $group = $group_conf->{"ch_". $GLOBALS['json_object']->channelid}->{$GLOBALS['json_object']->type};
		// if (is_null($group)) $group = $group_conf->base->{$GLOBALS['json_object']->type};
		// return $group;
	// }
	
	public function generatePlaylist () {
		$visitor = (preg_match("/^(lwweb|web|mobile)$/", @$GLOBALS['json_object']->visitor)) ? "mobile" : $GLOBALS['json_object']->visitor;
		require_once $GLOBALS['src_dir'] .'/configures/Blackoutconfig.php';
		$isBlackout = Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid);
		$query = !$isBlackout ? implode(";", array($GLOBALS['json_object']->appid, $GLOBALS['json_object']->langid, $GLOBALS['json_object']->type, $GLOBALS['json_object']->visitor, $GLOBALS['json_object']->streamlvl, "aes", $GLOBALS['json_object']->channelid)) : "htv;;live;mobile;auto;aes;bk";
		$default = !$isBlackout ? implode(";", array("htv", $GLOBALS['json_object']->langid, $GLOBALS['json_object']->type, $visitor, $GLOBALS['json_object']->streamlvl, "aes", $GLOBALS['json_object']->channelid)) : "htv;;live;mobile;auto;aes;bk";

		$return = $GLOBALS['redis']->getRedis($query, 1, array('host' => '10.18.19.98', 'port' => 6380));
		if ($return !== false) {
			$return = str_replace("yyyymmdd", date("Ymd", (int)@$GLOBALS['json_object']->stime), $return);
			// file_put_contents("/www/logs/generatePlaylist.sp2.log", $default ." >> ".$return."\n",FILE_APPEND);
			return "/".$return."/playlist.m3u8";
		}
		else {
			$return = $GLOBALS['redis']->getRedis($default, 1, array('host' => '10.18.19.98', 'port' => 6380));
			if ($return !== false) {
				$return = str_replace("yyyymmdd", date("Ymd", (int)@$GLOBALS['json_object']->stime), $return);
				// file_put_contents("/www/logs/generatePlaylist.sp2.log", $default ." >> ".$return."\n",FILE_APPEND);
				return "/".$return."/playlist.m3u8";
			}
			else return null;
		}
	}
	
	//  public function getrsaqstring () {
	// 	require_once $GLOBALS['src_dir'] .'/configures/Blackoutconfig.php';
	// 	require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
	// 	$channelid = ($GLOBALS['json_object']->channelid != '021') ? $GLOBALS['json_object']->channelid : '135';
	// 	$channelid = ($GLOBALS['json_object']->channelid != 'en107') ? $GLOBALS['json_object']->channelid : '107';
	// 	$dvr = ($GLOBALS['json_object']->type == "live") ? "live" : "dvr";
	// 	$stream = !Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid) ? $channelid : "bk";
	// 	$encrypt = Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $stream ."|". $GLOBALS['json_object']->uid ."|". $dvr);
		
	// 	if ($GLOBALS['json_object']->type == "timeshift") @$querystring = "dvr=&";
	// 	elseif ($GLOBALS['json_object']->type == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']->stime, "YmdHis")}&wowzadvrplaylistduration={$GLOBALS['json_object']->duration}&";
	// 	@$querystring .= "appid={$GLOBALS['json_object']->appid}&visitor={$GLOBALS['json_object']->visitor}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
	// 	return $querystring;
	// }
}
?>
