<?
include_once( dirname(__DIR__). '/Streamconfig2.php');
class Ptvconfig extends Streamconfig2 {
	public function __construct () {
		parent::__construct();
	}
	
	public function generatePlaylist () {
		$visitor = "mobile";
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'"  and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);

		if (count($ret_arr) > 0) {
			require_once $GLOBALS['src_dir'] .'/configures/Blackoutconfig.php';
			// var_dump($object);
			$smil = str_replace("yyyymmdd", date("Ymd", (int)$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
			if ($GLOBALS['json_object']->type == "live" && Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid)) $smil = "bk_m_auto.smil";
			## chromecast smil
			$smil = $GLOBALS['json_object']->visitor != "chromecast" ? $smil : str_replace("_m_", "_tv_", $smil);
			
			$appinst = "";
			switch ($GLOBALS['json_object']->type) {
				case "timeshift" :
					$appinst = "liveedge_app_ts";
				break;
				default :
					$appinst = (string)$ret_arr[0]->appinst;
				break;
			}
			return "/".$appinst."/". $smil ."/playlist.m3u8";
		}
		else return null;
	}
}
?>