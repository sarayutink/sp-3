<?
include_once( dirname(__DIR__). '/Streamconfig2.php');
class Samsungtvconfig extends Streamconfig2 {
	public function __construct () {
		parent::__construct();
	}
	
	public function generatePlaylist () {
		$visitor = "mobile";
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'"  and @device="'. $visitor .'"  and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);
		// var_dump($ret_arr);
		if (count($ret_arr) > 0) {
			require_once $GLOBALS['src_dir'] .'/configures/Blackoutconfig.php';
			// var_dump($object);
			$smil = str_replace("yyyymmdd", date("Ymd", (int)$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
			if ($GLOBALS['json_object']->type == "live" && Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid)) $smil = "bk_m_auto.smil";
			return "/".(string)$ret_arr[0]->appinst."/". $smil ."/playlist.m3u8";
		}
		else return null;
	}
	
	// public function getrsaqstring () {
		// require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		// $encrypt = Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $GLOBALS['json_object']->channelid ."|". $GLOBALS['json_object']->uid ."|". $GLOBALS['bizconf']->bizconf->user);
		
		// if ($GLOBALS['json_object']->type == "timeshift") @$querystring = "dvr=&";
		// elseif ($GLOBALS['json_object']->type == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']->stime, "YmdHis")}&wowzadvrplaylistduration={$GLOBALS['json_object']->duration}&";
		// @$querystring .= "appid={$GLOBALS['json_object']->appid}&visitor={$GLOBALS['json_object']->visitor}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		// return $querystring;
	// }
}
?>