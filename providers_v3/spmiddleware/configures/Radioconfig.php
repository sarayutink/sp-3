<?
class Radioconfig {
	var $bizconf;
	
	public function __construct () {
		$this->bizconf = $this->loadLocalXML($GLOBALS['src_dir'] .'/assets/radiostreamprovider/'. $GLOBALS['json_object']->appid .'.conf');
	}
	
	private function loadLocalXML ($filepath) {
		if (file_exists($filepath)) {
			/** load config in XML */
			try {
				$xml_obj = simplexml_load_file($filepath);
				return $xml_obj;
			}
			catch(Exception $e) {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public function generatePlaylist () {
		$cols = explode(".", $GLOBALS['json_object']->channelid);
		$return = $this->bizconf->appinst ."/". $cols[0] .".stream_aac";
		return ($GLOBALS['json_object']->visitor == "wap") ? $return : $return ."/playlist.m3u8";
	}
	
	public function getGroupId () {
		$return = $this->bizconf->group->{$GLOBALS['json_object']->visitor};
		return (string)$return;
	}
	
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		//require_once $GLOBALS['src_dir'] .'/libraries/ArsOpensslcryption.php';
		// $token_arr = array('oqxg55CptqALr0','aFEacfYcpOtkGi','famnCWFwjvlksk','5M1L5wcBblA0IX','oP9Qlqas8YJlvp','9UxURr9saLEd5F');
		$encrypt = Opensslcryption::encryptstr(time() ."-ppmps");
		//$encrypt = Arsopensslcryption::encryptstr(time() ."-ppmps");
		// $encrypt = Opensslcryption::encryptstr(time() ."-tvsrtsp");
		@$querystring .= "appid={$GLOBALS['json_object']->appid}&visitor={$GLOBALS['json_object']->visitor}&mpass={$encrypt}";
		
		return $querystring;
	}
}