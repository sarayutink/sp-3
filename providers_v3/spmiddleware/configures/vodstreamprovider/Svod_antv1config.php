<?
include_once( dirname(__DIR__). '/Vodconfig.php');
class Svod_antv1config extends Vodconfig {
	public function __construct () {
		parent::__construct();
	}
	
	// public function changeServer ($server) {
		// return true;
	// }
	
	// public function getGroupId () {
	// 	switch ($GLOBALS['json_object']->drm) {
	// 		case "wv" :
	// 			return 114;
	// 		break;
	// 		default :
	// 			return 15;
	// 		break;
	// 	}
	// }
	
	public function generatePlaylist () {
		$streamname = ($GLOBALS['json_object']->visitor == "stb") ? "_stb_auto.smil" : "_m_auto.smil";
		$streamname = str_pad($GLOBALS['json_object']->streamname, 12, "0", STR_PAD_LEFT) . $streamname;
		return "/". $this->bizconf->streammapping->AppInst ."/smil:". $streamname ."/playlist.m3u8";
	}
}
?>