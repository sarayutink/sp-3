<?
include_once( dirname(__DIR__). '/Vodconfig.php');
class Sportclip_ptvconfig extends Vodconfig {
	public function __construct () {
		parent::__construct();
	}
	
	public function generatePlaylist () {
		return "/". $this->bizconf->streammapping->AppInst ."/mp4:". $GLOBALS['json_object']->streamname ."/playlist.m3u8";
	}
	// 1476165779|testsessid|htv|172.22.222.74|135|testuid|unlimit
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". explode("_", $GLOBALS['json_object']->streamname)[0] ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		// $encrypt = Opensslcryption::encryptbypass();
		$audio = ($GLOBALS['json_object']->langid == "th" ? "0" : "1");
		$querystring = "audioindex={$audio}&appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
}
?>