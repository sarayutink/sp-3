<?
require_once 'Tvodstreamprovider.req';

class Tvodstreamprovider {
	public static function provide ($request, $response) {
		#$body = $request->getBody();
		$body = file_get_contents("php://input");
		$GLOBALS['ctrl_name'] = "Tvodstreamprovider";
		$GLOBALS['lbl_name'] = "lalaynya";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		
		## validate post json
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
			if (file_exists($GLOBALS['src_dir'] ."/configures/tvodstreamprovider/".$apconf.".php")) {
				require_once($GLOBALS['src_dir'] ."/configures/tvodstreamprovider/".$apconf.".php");
				$GLOBALS['bizconf'] = new $apconf();
				## get streaming server via load balancer
				// require_once($GLOBALS['src_dir'] ."/controllers/CDNZoneBalancer.php");
				// $actionURL = $GLOBALS['bizconf']->getBalencer();
				// $GLOBALS['json_object']->type = "vod";
				// $server = CDNZoneBalancer::findServerByIP($actionURL, $GLOBALS['json_object']->csip);
				require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
				$actionURL = $GLOBALS['bizconf']->getBalencer();
				$in_group = $GLOBALS['bizconf']->getGroupId();
				$server = Loadbalancecontrol::findServer($actionURL, $in_group);
				// require_once($GLOBALS['src_dir'] ."/controllers/Roundbalancecontrol.php");
				// $in_group = $GLOBALS['bizconf']->getGroupId();
				// $server = Roundbalancecontrol::findServer($in_group);
				if (intval($server) == 0) {
					require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol_dev.php");
					$action = Streamcontrol_dev::createStreamManifest();
					if(!is_null($action)) {
						$return = array('result_code' => 200, 'result_description' => "SUCCESS", 'streamurl' => "https://".$server.$action['manifest'], 'license' => $GLOBALS['bizconf']->bizconf->license->{$GLOBALS['json_object']->drm} ."?". $action['license']);
						Logger::writelog(array('result_code' => 200, 'result' => $return['streamurl'], 'license' => $return['license']));
					}
					else $return = array('result_code' => 430, 'result_description' => "Cannot find playlist.");
				}
				// elseif ($server == "406") $return = array('result_code' => 406, 'result_description' => "406");
				else $return = array('result_code' => 420, 'result_description' => "Cannot find streaming server.");
			}
			else {
				$return = array('result_code' => 600, 'result_description' => "Invalid request.");
				Logger::writelog(array('result_code' => 620, 'result' => "INVALID APPID"));
			}
		}
		else {
			$return = array('result_code' => 600, 'result_description' => "Invalid request.");
			Logger::writelog(array('result_code' => 650, 'result' => "INVALID PARAMETERS"));
		}
		
		// if(strpos($GLOBALS['json_object']->appid, "v2") <> false) $return['version'] = "2.0";
		return $return;
    }
}
