<?
define("SRCDIR", __DIR__);
define("LOGDIR", "/www/logs/providers");

global $sprdhost;
global $lbrdhost;

$sprdhost = "sprdhost";
$lbrdhost = "lbrdhost";

define("SPRDHOST", "sprdhost");
define("SPRDPORT", 6380);

define("LBRDHOST", "lbrdhost");
define("LBRDPORT", 6383);
define("LBQUOTA", 0.4);
define("CCU_LIMIT_BREAK", true);

define("BZRDHOST", "sprdhost");
define("BZRDPORT", 6379);
define("BZENABLE", true);


require_once SRCDIR .'/libraries/Mwredis.php';
global $rd_3;
$rd_3 = new Mwredis($sprdhost);

require_once SRCDIR .'/models/Variable.php';
require_once SRCDIR .'/models/Logger.php';

$json_object = '';
$ctrl_name = '';
$config = '';
