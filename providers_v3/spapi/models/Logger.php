<?
class Logger {
	private static function getOrderIndex () {
		return array("uid", "sessionid", "appid", "channelid", "streamname", "langid", "streamlvl", "type", "stime", "duration", "csip", "agent", "visitor", "referer", "drm", "ccucheck", "region");
	}
	
	public static function writelog ($result_arr) {
		$cdr_arr['date'] = date("[d/M/Y:H:i:s O]");
		// $cdr_arr['time'] = date("H:i:s");
		$cdr_arr['controller'] = $GLOBALS['ctrl_name'];
		$cdr_arr['desc'] = ($result_arr['result_code'] == 200 ? 'SUCCESS' : 'FAILED');
		$cdr_arr['result_code'] = $result_arr['result_code'];
		$cdr_arr['result'] = "\"".@$result_arr['result'].@$result_arr['streamurl']."\"";
		$cdr_arr['license'] = (isset($result_arr['license']) ? "\"".$result_arr['license']."\"" : "-");
		foreach (self::getOrderIndex() as $key) $cdr_arr[$key] = (!empty($GLOBALS['json_object'][$key]) ? $GLOBALS['json_object'][$key] : "-");
		$cdr_arr['csip'] = strrpos($cdr_arr['csip'], ".") ? $cdr_arr['csip'] : "-";
		$cdr_arr['agent'] = "\"".$cdr_arr['agent']."\"";
		
		$destination = LOGDIR ."/result_". date("Ymd_H") .".log";
		if (file_exists($destination)) file_put_contents($destination, implode('  ', $cdr_arr) ."\n", FILE_APPEND);
		else {
			file_put_contents(
				$destination, 
				"#Fields: date  controller  desc  result_code result  license uid  sessionid  appid  channelid  streamname  langid  streamlvl  type  stime  duration  csip  agent  visitor  referer  drm  ccucheck  region\n", FILE_APPEND);
			file_put_contents($destination, implode('  ', $cdr_arr) ."\n", FILE_APPEND);
		}
	}

	public static function sendmsg ($result_arr) {
		$cdr_arr['date'] = date("[d/M/Y:H:i:s O]");
		// $cdr_arr['time'] = date("H:i:s");
		$cdr_arr['controller'] = $GLOBALS['ctrl_name'];
		$cdr_arr['desc'] = ($result_arr['result_code'] == 200 ? 'SUCCESS' : 'FAILED');
		$cdr_arr['result_code'] = $result_arr['result_code'];
		$cdr_arr['result'] = "\"".@$result_arr['result'].@$result_arr['streamurl']."\"";
		$cdr_arr['license'] = (isset($result_arr['license']) ? "\"".$result_arr['license']."\"" : "-");
		foreach (self::getOrderIndex() as $key) $cdr_arr[$key] = (!empty($GLOBALS['json_object'][$key]) ? $GLOBALS['json_object'][$key] : "-");
		$cdr_arr['csip'] = strrpos($cdr_arr['csip'], ".") ? $cdr_arr['csip'] : "-";
		$cdr_arr['agent'] = "\"".$cdr_arr['agent']."\"";
	}
}