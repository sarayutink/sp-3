<?
class Variable {
	private static $visitor = "|web|mobile|chromecast|websport|lwweb|tv|antv|";
	private static $streamlvl = "|auto|hd|high|medium|low|";
	private static $type = "|live|timeshift|catchup|tvod|svod|clip|";
	private static $drm = "|aes|wv|fp|";
	private static $langid = "|th|en|";
		
	public static function validate () {
		switch($GLOBALS['ctrl_name']) {
			case 'livestream' :
				$check = 0;
				$check += !preg_match("/\w{3,10}/", @$GLOBALS['json_object']['appid']) ? 0.1 : 0;
				$check += !preg_match("/\w{0,30}/", @$GLOBALS['json_object']['uid']) ? 0.01 : 0;
				$check += !preg_match("/\w{0,15}/", @$GLOBALS['json_object']['sessionid']) ? 0.05 : 0;
				$check += !preg_match("/\w{3,7}/", @$GLOBALS['json_object']['channelid']) ? 0.001 : 0;
				// $check += !is_int(strpos(self::$langid, "|".@$GLOBALS['json_object']['langid']."|")) ? 1 : 0;
				$check += !is_int(strpos(self::$streamlvl, "|".@$GLOBALS['json_object']['streamlvl']."|")) ? 10 : 0;
				$check += !is_int(strpos(self::$type, "|".@$GLOBALS['json_object']['type']."|")) ? 5 : 0;
				$check += !preg_match("/[a-zA-Z0-9 ,._-]{0,}/", @$GLOBALS['json_object']['agent']) ? 0.0001 : 0;
				$check += !is_int(strpos(self::$visitor, "|".@$GLOBALS['json_object']['visitor']."|")) ? 50 : 0;
				$check += !is_int(strpos(self::$drm, "|".@$GLOBALS['json_object']['drm']."|")) ? 100 : 0;
				
				$GLOBALS['json_object']['uid'] = self::generateRandomString($GLOBALS['json_object']['uid']) ?: $GLOBALS['json_object']['uid'];
				$GLOBALS['json_object']['sessionid'] = self::generateRandomString($GLOBALS['json_object']['sessionid']) ?: $GLOBALS['json_object']['sessionid'];
				$GLOBALS['json_object']['csip'] = !empty($GLOBALS['json_object']['csip']) ? $GLOBALS['json_object']['csip'] : "0.0.0.0";
				
				return $check == 0;
			break;
			case 'vodstream' :
				$check = 0;
				$check += !preg_match("/\w{3,10}/", @$GLOBALS['json_object']['appid']) ? 0.1 : 0;
				$check += !preg_match("/\w{0,30}/", @$GLOBALS['json_object']['uid']) ? 0.01 : 0;
				$check += !preg_match("/\w{0,15}/", @$GLOBALS['json_object']['sessionid']) ? 0.05 : 0;
				$check += !preg_match("/[a-zA-Z0-9._]{1,}/", @$GLOBALS['json_object']['streamname']) ? 0.005 : 0;
				// $check += !is_int(strpos(self::$langid, "|".@$GLOBALS['json_object']['langid']."|")) ? 1 : 0;
				$check += !is_int(strpos(self::$streamlvl, "|".@$GLOBALS['json_object']['streamlvl']."|")) ? 10 : 0;
				$check += !is_int(strpos(self::$type, "|".@$GLOBALS['json_object']['type']."|")) ? 5 : 0;
				$check += !preg_match("/[a-zA-Z0-9 ,._-]{0,}/", @$GLOBALS['json_object']['agent']) ? 0.0001 : 0;
				$check += !is_int(strpos(self::$visitor, "|".@$GLOBALS['json_object']['visitor']."|")) ? 50 : 0;
				$check += !is_int(strpos(self::$drm, "|".@$GLOBALS['json_object']['drm']."|")) ? 100 : 0;
				
				$GLOBALS['json_object']['uid'] = self::generateRandomString($GLOBALS['json_object']['uid']) ?: $GLOBALS['json_object']['uid'];
				$GLOBALS['json_object']['sessionid'] = self::generateRandomString($GLOBALS['json_object']['sessionid']) ?: $GLOBALS['json_object']['sessionid'];
				$GLOBALS['json_object']['csip'] = !empty($GLOBALS['json_object']['csip']) ? $GLOBALS['json_object']['csip'] : "0.0.".self::generateRandomString(false);
				
				return $check == 0;
			break;
			case 'radiostream' :
				$check = 0;
				$check += !preg_match("/\w{3,10}/", @$GLOBALS['json_object']['appid']) ? 0.1 : 0;
				$check += !preg_match("/\w{0,30}/", @$GLOBALS['json_object']['uid']) ? 0.01 : 0;
				$check += !preg_match("/\w{0,15}/", @$GLOBALS['json_object']['sessionid']) ? 0.05 : 0;
				$check += !preg_match("/[a-zA-Z0-9 ,._-]{0,}{3,}/", @$GLOBALS['json_object']['channelid']) ? 0.001 : 0;
				// $check += !is_int(strpos(self::$langid, "|".@$GLOBALS['json_object']['langid']."|")) ? 1 : 0;
				$check += !is_int(strpos(self::$streamlvl, "|".@$GLOBALS['json_object']['streamlvl']."|")) ? 10 : 0;
				$check += !is_int(strpos(self::$type, "|".@$GLOBALS['json_object']['type']."|")) ? 5 : 0;
				$check += !preg_match("/[a-zA-Z0-9 ,._-]{0,}/", @$GLOBALS['json_object']['agent']) ? 0.0001 : 0;
				$check += !is_int(strpos(self::$visitor, "|".@$GLOBALS['json_object']['visitor']."|")) ? 50 : 0;
				$check += !is_int(strpos(self::$drm, "|".@$GLOBALS['json_object']['drm']."|")) ? 100 : 0;
				
				$GLOBALS['json_object']['uid'] = self::generateRandomString($GLOBALS['json_object']['uid']) ?: $GLOBALS['json_object']['uid'];
				$GLOBALS['json_object']['sessionid'] = self::generateRandomString($GLOBALS['json_object']['sessionid']) ?: $GLOBALS['json_object']['sessionid'];
				$GLOBALS['json_object']['csip'] = !empty($GLOBALS['json_object']['csip']) ? $GLOBALS['json_object']['csip'] : "0.0.".self::generateRandomString(false);
				
				return $check == 0;
			break;
			default :
				# undefined controller
				return false;
			break;
		}
	}
	
	private static function generateRandomString($param, $length = 10) {
		switch(empty($param)) {
			case false:
				return false;
			break;
			default:
				return str_shuffle(substr(str_repeat(md5(mt_rand()), 2+$length/32), 0, $length));
			break;
		}
	} 
}