<?
class Livestreamconf {
	private $isBlackout;

    public function isCCOver () {
		require_once SRCDIR .'/configures/Businesconf.php';
		$bizconf = new Businesconf();
		return $bizconf->isCCuOver($GLOBALS['json_object']['uid'], $GLOBALS['json_object']['sessionid']) && @$GLOBALS['json_object']['ccucheck'];
  	}
	
	public function isBlacklist () {
        return !is_bool($GLOBALS['rd_3']->getRedis($GLOBALS['json_object']['uid'], 7));
    }
    
    #"all;th;live;mobile;medium;wv;184"
	public function generatePlaylist () {
        $redis = new Redis();
		$return = array("result_code" => 433, "result" => "EMPTY_LIVE_MAPPING");
		$query = "";
        try {
            $redis->connect(SPRDHOST, SPRDPORT);

            # isBlackout
            $this->isBlackout = false;
            $redis->select(3);
            $lists = $redis->keys("*".$GLOBALS['json_object']['appid'].";".$GLOBALS['json_object']['channelid']."*");
            if (count($lists) > 0) {
                arsort($lists);
                $tmstmp = time();
                foreach ($lists as $list) {
                    $row = explode(";", $list);
                    if ($row[1] <= $tmstmp && $tmstmp <= $row[0]) {
                        // var_dump($list);
                        $this->isBlackout = true;
                    }
                }
            }

			# mapping smil
			$chid = !$this->isBlackout ? $GLOBALS['json_object']['channelid'] : "bk";
            $query = implode(";", array($GLOBALS['json_object']['appid'], $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], $GLOBALS['json_object']['visitor'], $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], $chid));
            $default = implode(";", array("all", $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], "all", $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], $chid));

            $redis->select(1);
            $smils = $redis->mget([$query, $default]);
            if ($smils[0] !== false) $return = array("result_code" => 200, "result" => $smils[0]);
            else {
                if ($smils[1] !== false) $return = array("result_code" => 200, "result" => $smils[1]);
                else $return = array("result_code" => 433, "result" => "EMPTY_LIVE_MAPPING", "QUERY" => $query);
            }

            $redis->close();
            
            if ($GLOBALS['json_object']['type'] == "catchup" && $return <> false) {
                if (strpos($return['result'], "yyyymmdd_HHmmss")) $return = str_replace("yyyymmdd_HHmmss", $this->changeDateFormat($GLOBALS['json_object']['stime'], "Ymd_His"), $return);
                elseif (strpos($return['result'], "yyyymmdd")) $return = str_replace("yyyymmdd", $this->changeDateFormat($GLOBALS['json_object']['stime'], "Ymd"), $return);
                else array("result_code" => 433, "result" => "EMPTY_LIVE_MAPPING");
            }
        }
		catch (Exception $e) {
            $return = array("result_code" => 439, "result" => "REDIS_LIVE_MAPPING_WENT_AWAY");
            file_put_contents(LOGDIR ."/generatePlaylist.err.log", date("Y/m/d_H:i:s") ."  ". SPRDHOST.":".SPRDPORT ."  ". $e->getMessage()."\n", FILE_APPEND);
        }

        return $return;
    }
	
	public function getrsaqstring ($bypass = false) {
        $channelid = $GLOBALS['json_object']['channelid'] == "en107" ? "107" : $GLOBALS['json_object']['channelid'];
        $channelid = !$this->isBlackout ? $channelid : "bk";
        $type = $GLOBALS['json_object']['type'] == "live" ? "live" : "dvr";
        $mpass = $bypass === false ? Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']['sessionid'] ."|". $GLOBALS['json_object']['appid'] ."|". $GLOBALS['json_object']['csip'] ."|". $channelid ."|". $GLOBALS['json_object']['uid'] ."|". $type) : Opensslcryption::encryptbypass();

		if ($GLOBALS['json_object']['type'] == "timeshift") @$querystring = "dvr=&";
		// elseif ($GLOBALS['json_object']['type'] == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']['stime'], "YmdHis")}&wowzadvrplaylistduration={$GLOBALS['json_object']['duration']}&";
		@$querystring .= "appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&uid={$GLOBALS['json_object']['uid']}&mpass={$mpass}";
		
		return $querystring;
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
    }
}
