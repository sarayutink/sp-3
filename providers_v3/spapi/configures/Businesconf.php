<?
class Businesconf {
    private $redis;

	public function __Construct () {
		$this->redis = new Redis();
	}

    public function isBlacklist ($ssoid) {
        $exist = $this->redisCtrl(4, "exists", array($ssoid));
        return $exist;
    }

    public function isCCuOver ($ssoid, $device) {
        $return = false;
        if (BZENABLE) {
            try {
                $this->redis->connect(BZRDHOST, BZRDPORT);
                $this->redis->select(2);
                $white = $this->redis->exists($ssoid);
                if ($white == 0) {
                    $this->redis->select(3);
                    $device = count($this->redis->keys($ssoid."|".$device."|*"));
                    $ssoid = count($this->redis->keys($ssoid."|*"));
                    $this->redis->close();
                    if ($ssoid > 0) $return = ($device > 0 ? false : true);
                    else $return = false;
                }
                else {
                    $this->redis->close();
                    $return = false;
                }
            }
            catch (Exception $e) {
                // echo $e->getMessage() ."\n";
                newrelic_notice_error($e);
                return null;
            }
        }
        
        return $return;
    }

    private function redisCtrl ($redisdb, $func, $params) {
        try {
            $this->redis->connect(BZRDHOST, BZRDPORT);
            $this->redis->select($redisdb);
            $return = $this->redis->$func(...$params);
            $this->redis->close();

            return $return;
        }
		catch (Exception $e) {
            // echo $e->getMessage() ."\n";
            newrelic_notice_error($e);
            return null;
		}
    }
}