<?
class CDNPoolBalancer {	
    private $subnet;
	
	public function findCDNByIP () {
        $redis = new Redis();
        $host = $GLOBALS['sprdhost']; $port = SPRDPORT;
        $group = 200;
		try {
            $redis->connect($host, $port);
        
			$separate_ip_str = explode(".", $GLOBALS['json_object']['csip']);
			$redis->select(5);
			$subnets = $redis->keys($separate_ip_str[0].".".$separate_ip_str[1].".*");
			if (count($subnets) > 0) array_walk($subnets, array($this,'search_subnet'));
			$this->subnet = is_null($this->subnet) ? "other" : $this->subnet;
			$networkname = $redis->get($this->subnet);
			$redis->select(4);
			$chgroup = $redis->get(@$GLOBALS['json_object']['channelid'].@$GLOBALS['json_object']['streamname']) ?: "local";
			$charging = @!$GLOBALS['json_object']['charge'] ? "free" : "charge";
			
			$getKey = $networkname .";". $charging .";". $GLOBALS['json_object']['appid'] .";". $chgroup .";". $GLOBALS['json_object']['visitor'] .";". $GLOBALS['json_object']['type'] .";". $GLOBALS['json_object']['streamlvl'] .";". $GLOBALS['json_object']['drm'];

			$redis->select(6);
            $group = $redis->get($getKey);
			$redis->close();
		} catch (Exception $e) {
			file_put_contents(LOGDIR ."/cdn_pool_balancer_".date("Ymd_H").".err", date("Y/m/d_H:i:s") ."  ". $host.":".$port ."  ". $e->getMessage()."\n", FILE_APPEND);
        }
        
        require_once SRCDIR .'/libraries/Demeter.php';
        $lb = new Demeter();
        $groupchain = explode(",", "200");
        $cdn;
        foreach ($groupchain as $row) {
            $cdn = $lb->findCDN($row, true);
            if ($cdn !== false) break;
        }

        return $cdn !== false ? $cdn : serialize(array('result_code' => 421, 'result' => 'NO_CDN_IN_GORUP_CHAIN'));
    }
    
    private function search_subnet ($item) {
        $this->cidr_match($GLOBALS['json_object']['csip'], $item);
    }

    private function cidr_match($ip, $range)
    {
        list ($subnet, $bits) = explode('/', $range);
        if ($bits === null) {
            $bits = 32;
        }
        $ip = ip2long($ip);
        $subnet = ip2long($subnet);
        $mask = -1 << (32 - $bits);
        $subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
        if (($ip & $mask) == $subnet) $this->subnet = $range;
    }
}
?>