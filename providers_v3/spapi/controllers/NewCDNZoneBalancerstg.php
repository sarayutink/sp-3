<?
class NewCDNZoneBalancerstg {	
    private $subnet;
	
	public function findCDNByIP () {
        $redis = new Redis();
        $host = $GLOBALS['sprdhost']; $port = SPRDPORT;
        $group = 200;
		try {
            $redis->connect($host, $port);
        
			$separate_ip_str = explode(".", $GLOBALS['json_object']['csip']);
			$redis->select(5);
			$subnets = $redis->keys($separate_ip_str[0].".".$separate_ip_str[1].".*");
			if (count($subnets) > 0) array_walk($subnets, array($this,'search_subnet'));
			$this->subnet = is_null($this->subnet) ? "other" : $this->subnet;
			$networkname = $redis->get($this->subnet);
			$redis->select(4);
            $chgroup = $redis->get(@$GLOBALS['json_object']['channelid'].@$GLOBALS['json_object']['streamname']) ?: "local";
            require_once "/www/webapps/providers_v3/spapi/libraries/Gaia.php";
            // file_put_contents(LOGDIR ."/Gaia.debug", date("Y/m/d_H:i:s") ."  ". $GLOBALS['json_object']['csip'] .":". Gaia::getCountry($GLOBALS['json_object']['csip']) ."\n", FILE_APPEND);
            $charging = Gaia::getCountry($GLOBALS['json_object']['csip']) == "TH" ? "free" : "charge";
			// $charging = @!$GLOBALS['json_object']['charge'] ? "free" : "charge";
			
			$getKey = $networkname .";". $charging .";". $GLOBALS['json_object']['appid'] .";". $chgroup .";". $GLOBALS['json_object']['visitor'] .";". $GLOBALS['json_object']['type'] .";". $GLOBALS['json_object']['streamlvl'] .";". $GLOBALS['json_object']['drm'];

			$redis->select(6);
            $group = $redis->get($getKey);
			$redis->close();
		} catch (Exception $e) {
			file_put_contents(LOGDIR ."/NewCDNZoneBalancer.err", date("Y/m/d_H:i:s") ."  ". $host.":".$port ."  ". $e->getMessage()."\n", FILE_APPEND);
		}

        // $host = "10.18.19.157"; $port = 6383;
        // $redis->connect($host, $port);
        // $redis->select($groupchain);
        // $cdns = $redis->keys("*");
        // $redis->close($host, $port);
        // $random_keys=array_rand($cdns, 1);
        // return $cdns[$random_keys];
        require_once SRCDIR .'/libraries/Lalaynya.php';
        $lb = new Lalaynya();
        $cdn = $lb->findCDN($group, true);
        if (is_int(strpos($cdn, "true"))) return $cdn;

        return serialize(array('result_code' => 421, 'result' => 'NO_CDN_IN_GORUP_CHAIN'));
    }
    
    private function search_subnet ($item) {
        $this->cidr_match($GLOBALS['json_object']['csip'], $item);
    }

    private function cidr_match($ip, $range)
    {
        list ($subnet, $bits) = explode('/', $range);
        if ($bits === null) {
            $bits = 32;
        }
        $ip = ip2long($ip);
        $subnet = ip2long($subnet);
        $mask = -1 << (32 - $bits);
        $subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
        if (($ip & $mask) == $subnet) $this->subnet = $range;
    }
}
?>