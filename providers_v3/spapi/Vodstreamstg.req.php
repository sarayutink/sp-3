<?
$apconf = ucfirst($GLOBALS['json_object']['appid']);
$GLOBALS['config'] = SRCDIR."/configures/{$GLOBALS['json_object']['type']}stream/{$apconf}.php";
if (file_exists($GLOBALS['config'])) {
    require_once $GLOBALS['config'];
    $GLOBALS['config'] = new $apconf();
}
else {
    require_once SRCDIR."/configures/Vodstreamstgconf.php";
    $GLOBALS['config'] = new Vodstreamstgconf();
}
require_once SRCDIR.'/libraries/ArsOpensslcryption.php';
require_once SRCDIR.'/libraries/Opensslcryption.php';
require_once SRCDIR.'/controllers/NewCDNZoneBalancer.php';