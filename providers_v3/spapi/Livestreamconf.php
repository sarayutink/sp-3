<?
class Livestreamconf {	
	public function isBlacklist () {
		return !is_bool($GLOBALS['rd_3']->getRedis($GLOBALS['json_object']['uid'], 7));
	  }
	
	public function isCCOver () {
		if (@$GLOBALS['json_object']['ccucheck'] == true) {
			require_once SRCDIR .'/configures/Businesconf.php';
			$bizconf = new Businesconf();
			return $bizconf->isCCuOver($GLOBALS['json_object']['uid'], $GLOBALS['json_object']['sessionid']);
		}
		else return false;
	}
    
    #"all;th;live;mobile;medium;wv;184"
	public function generatePlaylist () {
		$isBlackout = $this->isBlackout($GLOBALS['json_object']['appid'], $GLOBALS['json_object']['channelid']);
        $query = !$isBlackout ? implode(";", array($GLOBALS['json_object']['appid'], $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], $GLOBALS['json_object']['visitor'], $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], $GLOBALS['json_object']['channelid'])) : implode(";", array($GLOBALS['json_object']['appid'], $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], $GLOBALS['json_object']['visitor'], $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], "bk"));
        $default = !$isBlackout ? implode(";", array("all", $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], $GLOBALS['json_object']['visitor'], $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], $GLOBALS['json_object']['channelid'])) : implode(";", array("all", $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], $GLOBALS['json_object']['visitor'], $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], "bk"));
        
// file_put_contents("/www/logs/providers/generatePlaylist.log", $GLOBALS['json_object']['channelid'] . " : " . $query ." , ".$default, FILE_APPEND);
        $query = $GLOBALS['rd_3']->getRedis($query, 1);
        $default = $GLOBALS['rd_3']->getRedis($default, 1);
        $return = (empty($query) ? (!empty($default) ? $default : false) : $query);
// file_put_contents("/www/logs/providers/generatePlaylist.log", " >> " . $query ." , ".$default."\n", FILE_APPEND);
        if ($GLOBALS['json_object']['type'] == "catchup" && $return <> false) $return = str_replace("yyyymmdd", $this->changeDateFormat($GLOBALS['json_object']['stime'], "Ymd"), $return);

         return $return;
    }
	
	public function getrsaqstring ($bypass = false) {
        $channelid = $GLOBALS['json_object']['channelid'] == "en107" ? "107" : $GLOBALS['json_object']['channelid'];
        $channelid = !$this->isBlackout($GLOBALS['json_object']['appid'], $GLOBALS['json_object']['channelid']) ? $channelid : "bk";
        $type = $GLOBALS['json_object']['type'] == "live" ? "live" : "dvr";
        $mpass = $bypass === false ? Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']['sessionid'] ."|". $GLOBALS['json_object']['appid'] ."|". $GLOBALS['json_object']['csip'] ."|". $channelid ."|". $GLOBALS['json_object']['uid'] ."|". $type) : Opensslcryption::encryptbypass();

		if ($GLOBALS['json_object']['type'] == "timeshift") @$querystring = "dvr=&";
		elseif ($GLOBALS['json_object']['type'] == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']['stime'], "YmdHis")}&wowzadvrplaylistduration={$GLOBALS['json_object']['duration']}&";
		@$querystring .= "appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&uid={$GLOBALS['json_object']['uid']}&mpass={$mpass}";
		
		return $querystring;
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
    }
    
    public function isBlackout ($appid, $chid) {
		$lists = $GLOBALS['rd_3']->getRedis("*{$appid};{$chid}*", 3);
		if (count($lists) > 0) {
			arsort($lists);
			$tmstmp = time();
			foreach ($lists as $list) {
				$row = explode(";", $list);
				if ($row[1] <= $tmstmp && $tmstmp <= $row[0]) {
					// var_dump($list);
					return true;
				}
			}
			return false;
		}
		else return false;
	}
}