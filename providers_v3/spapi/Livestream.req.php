<?
$apconf = ucfirst($GLOBALS['json_object']['appid']);
$GLOBALS['config'] = SRCDIR."/configures/livestream/{$apconf}.php";
if (file_exists($GLOBALS['config'])) {
    require_once $GLOBALS['config'];
    $GLOBALS['config'] = new $apconf();
}
else {
    require_once SRCDIR."/configures/Livestreamconf.php";
    $GLOBALS['config'] = new Livestreamconf();
}
require_once SRCDIR.'/libraries/ArsOpensslcryption.php';
require_once SRCDIR.'/libraries/Opensslcryption.php';
require_once SRCDIR.'/controllers/NewCDNZoneBalancer.php';