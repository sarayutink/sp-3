<?
class Opensslcryption {
	public static function encrypt ($strtoencrypt) {
		$iv = openssl_random_pseudo_bytes(16);
		$encryption_key = 'TVS-PPM-01067541';

		$data = openssl_encrypt($strtoencrypt, 'aes-128-cbc', $encryption_key, OPENSSL_RAW_DATA , $iv);
		
		return urlencode(str_replace("=", "", base64_encode($data ."::". $iv)));
	}
	
	public static function encryptbypass () {
		$token_arr = array('aDR81TY2wbl5G3bD', 'o54YDHDpHOLxlDl0', 'mTPiMyHNGwGIjEqY', 'EjixkniGQJ51ufjD', 'gUdwhojxwe5MrdbO');
		
		$iv = openssl_random_pseudo_bytes(16);
		$encryption_key = 'TVS-PPM-01067541';

		$data = openssl_encrypt($token_arr[rand(0,count($token_arr)-1)], 'aes-128-cbc', $encryption_key, OPENSSL_RAW_DATA , $iv);
		
		return urlencode(str_replace("=", "", base64_encode($data ."::". $iv)));
	}
	
	public static function decrypt ($strtodecrypt) {
		$encryption_key = 'TVS-PPM-01067541';
		$strtodecrypt = explode("::", $strtodecrypt);
		
		return openssl_decrypt($strtodecrypt[0], 'aes-128-cbc', $encryption_key, OPENSSL_RAW_DATA , $strtodecrypt[1]);
	}
}