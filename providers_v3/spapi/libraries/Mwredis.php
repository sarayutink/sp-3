<?
class Mwredis {
	private $redis;
	private $host;
	private $port = SPRDPORT;
	
	public function __Construct ($host) {
		$this->redis = new Redis();
		$this->host = $host;
	}
	
	public function setRedis ($uid, $value, $db = 0, $expire = null) {
		try {
			$this->redis->connect($this->host, $this->port);
			$this->redis->select($db);
			$this->redis->set($uid, $value);
			if (!empty($expire)) $this->redis->expire($uid, $expire);
			$this->redis->close();
		} 
		catch (Exception $e) {
			// echo $e ."\n";
			// echo "uid : $uid , value : $value , db : $db , expire : $expire \n";
		}
	}

	public function getRedis ($uid = "*", $db = 0) {
		try {
			$this->redis->connect($this->host, $this->port);
			$this->redis->select($db);
			$result = strpos($uid, "*") !== false ? $this->redis->keys($uid) : $this->redis->get($uid);
			$this->redis->close();
			return $result;
		} 
		catch (Exception $e) {
			// echo $e ."\n";
			// echo "uid : $uid , db : $db \n";
		}
	}

	public function delRedis ($uid, $db = 0) {
		try {
			$this->redis->connect($this->host, $this->port);
			$this->redis->select($db);
			$this->redis->del($uid);
			$this->redis->close();
		} 
		catch (Exception $e) {
			// echo $e ."\n";
			// echo "uid : $uid , db : $db \n";
		}
	}

	public function flushRedis ($db) {
		try {
			$this->redis->connect($this->host, $this->port);
			$this->redis->select($db);
			$this->redis->flushdb();
			$this->redis->close();
		} 
		catch (Exception $e) {
			// echo $e ."\n";
			// echo "db : $db \n";
		}
	}

	public function getDB ($appid, $mode = 0) {
		/** mode = 0 - get current number for bg process */
		if ($mode === 0) $return = $this->getRedis($appid, 0);
		/** mode = 1 - get next number for bg process */
		else if ($mode === 1) {
			$curr = $this->getRedis($appid, 0);
			$return = ($curr%2 == 0) ? $curr - 1 : $curr + 1;
			unset($curr);
		}
		return $return;
	}
}
