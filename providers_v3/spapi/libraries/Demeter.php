<?php
class Demeter {
        private $redisClient; private $cdnsdesc;

        public function findCDN ($groupname) {
            $host = $GLOBALS['sprdhost'];
            $port = LBRDPORT;
            $db = 8;
            $return = array("result_code" => 429, "result" => "REDIS_CDN_MEMBER_WENT_AWAY");
            try {
                $redis = new Redis();
                $redis->connect($host, $port);
                $redis->select($db);
                $this->cdnsdesc = $redis->get($groupname);
                $redis->close();
            } catch (Exception $e) {
                file_put_contents(__DIR__."/cdnselector-err.log", date("Y-m-d_H:i:s", time())."  ".$host.":".$port.":".$db."  ".$e->getMessage()."\n", FILE_APPEND);
                return array("result_code" => 429, "result" => "REDIS_CDN_MEMBER_AWAY");
            }
            $cdns_arr = json_decode($this->cdnsdesc, true);
            switch (count($cdns_arr)) {
                case 0:
                    $return = array("result_code" => 422, "result" => "[$groupname]_IS_EMPTY");
                    break;
                case 1 :
                    $return = array("result_code" => 200, "result" => $cdns_arr[0]['host']);
                    break;
                case 2 :
                    $return = array("result_code" => 200, "result" => $cdns_arr[array_rand($cdns_arr)]['host']);
                    break;
                default :
                    $samples = array_rand($cdns_arr, 2);
                    $result = $cdns_arr[$samples[0]]['ccu_available'] > $cdns_arr[$samples[1]]['ccu_available'] ? $cdns_arr[$samples[0]]['host'] : $cdns_arr[$samples[1]]['host'];
                    $return = array("result_code" => 200, "result" => $result);
                    break;
            }
            return $return;
        }
}