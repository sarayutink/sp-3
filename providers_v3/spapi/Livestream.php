<?
require_once __DIR__.'/config.req.php';

class Livestream {
	public static function provide () {
		$GLOBALS['ctrl_name'] = "livestream";
		$GLOBALS['json_object'] = json_decode(file_get_contents("php://input"), true);
		## validate post json
		require_once __DIR__."/Livestream.req.php";
		$isBlacklist = $GLOBALS['config']->isBlacklist();
		$isCCuOver = $GLOBALS['config']->isCCOver();
		if (Variable::validate() && !$isBlacklist && !$isCCuOver) {
			$qstring = $GLOBALS['config']->getrsaqstring();
			$livemaping = $GLOBALS['config']->generatePlaylist();
			$is_livemaping = ($livemaping['result_code'] == 200);
			$lb = new NewCDNZoneBalancer();
			$cdn = $lb->findCDNByIP();
			$is_cdn = ($cdn['result_code'] == 200);

			switch($GLOBALS['json_object']['drm']) {
				case "aes":
					if ($is_livemaping && $is_cdn) {
						$protocol = "http";
						$return['result_code'] = 200;
						$return['result'] = "$protocol://". $cdn['result'] ."/". $livemaping['result'] ."/playlist.m3u8?". $qstring;
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						// $return['result_code'] = $livemaping['result_code'];
						$return['result'] = "Cannot find playlist.";
						newrelic_notice_error(serialize($livemaping));
						Logger::writelog($livemaping);
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						// $return['result_code'] = $cdn['result_code'];
						$return['result'] = "Cannot find streaming server.";
						newrelic_notice_error(serialize($cdn));
						Logger::writelog($cdn);
					}
					$return['version'] = "3.0";
				break;
				case "wv":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['streamurl'] = "https://". $cdn['result'] ."/". $livemaping['result'] ."/manifest.mpd?appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&uid={$GLOBALS['json_object']['uid']}";
						$return['license'] = "https://kd.stm.trueid.net/charybdis/drmdecrypt?". $qstring;
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						// $return['result_code'] = $livemaping['result_code'];
						$return['result'] = "Cannot find playlist.";
						newrelic_notice_error(serialize($livemaping));
						Logger::writelog($livemaping);
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						// $return['result_code'] = $cdn['result_code'];
						$return['result'] = "Cannot find streaming server.";
						newrelic_notice_error(serialize($cdn));
						Logger::writelog($cdn);
					}
					$return['version'] = "3.0";
				break;
				case "fp":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['streamurl'] = "https://". $cdn['result'] ."/". $livemaping['result'] ."/playlist.m3u8?appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&uid={$GLOBALS['json_object']['uid']}";
						$return['license'] = "https://kd.stm.trueid.net/scylla/newdrmdecrypt?". $qstring;
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						// $return['result_code'] = $livemaping['result_code'];
						$return['result'] = "Cannot find playlist.";
						newrelic_notice_error(serialize($livemaping));
						Logger::writelog($livemaping);
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						// $return['result_code'] = $cdn['result_code'];
						$return['result'] = "Cannot find streaming server.";
						newrelic_notice_error(serialize($cdn));
						Logger::writelog($cdn);
					}
					$return['version'] = "3.0";
				break;
				default:
					$return = array('result_code' => 630, 'result' => "Invalid DRM request.");
					newrelic_notice_error(serialize($return));
					Logger::writelog($return);
				break;
			}
		}
		elseif ($isBlacklist) {
			$return = array('result_code' => 210, 'result' => "https://thumbnail.stm.trueid.net/blacklist/playlist.m3u8");
			Logger::writelog($return);
		}
		elseif ($isCCuOver) {
			$return = array('result_code' => 220, 'result' => "https://thumbnail.stm.trueid.net/ccu_limitation/playlist.m3u8");
			Logger::writelog($return);
		}
		else {
			$return = array('result_code' => 600, 'result' => "Invalid request.");
			newrelic_notice_error(serialize($return));
			Logger::writelog($return);
		}
		
		
		return $return;
    }
}
