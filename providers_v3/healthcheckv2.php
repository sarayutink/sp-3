<?php
function is_establish_redis ($host = '10.18.19.127', $port = '6380', $db = 1) {
	$redisexestarttime = microtime(true);
	$redis = new Redis();
	$redis->connect($host, $port);
	$redis->select($db);
	$return = $redis->keys("*");
	$redis->close();
	$redisexeendtime = microtime(true);
	$redisexetime = $redisexeendtime - $redisexestarttime;
	return array("value" => !empty($return), "exetime" => $redisexetime);
}

function is_establish_balancer ($url = "http://server_loadbalan:8080/lalaynya.php") {
	$lblexestarttime = microtime(true);
	$url .= "?group=11&uid=healthcheckv2";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_USERAGENT, "Streaming Provider 3 ($url)");
	if(curl_errno($curl)) $ret = false;
	else $ret = true;
	curl_close($curl);
	$lblexeendtime = microtime(true);
	$lblexetime = $lblexeendtime - $lblexestarttime;
	return array("value" => $ret, "exetime" => $lblexetime);
}

function is_establish_service () {
	
	//$ch_arr = array("001","007","008","024","027","028","034","040","045","046","55","055","056","057","058","060","061","062","068","069","072","073","074","075","081","088","089","091","094","095","096","097","101","105","107","108","109","114","116","117","121","122","127","128","135","139","141","142","143","145","152","159","163","164","165","166","171","173","174","176","177","178","191","192","193","207","208","211","214","218","221","222","224","225","c03","c05","c07","c09","c11","c12","ca03","ca05","d05","d11","d20","d24","d43","d56","d62","d75","d78","d83","da0","da1","da2","da7","da8","da9","db0","db1","db2","db3","db4","db5","db6","db7","db8","db9","dc0","dc1","dc2","ev03","ev05","he001","he002","ht001","ht002","ht003","ht004","ht005","ht006","ht007","ht008","ht009","ht111","ht112","ht113","ht114","ht115","ht116","ht117","ht118","o002","o014","o015","o016","o017","o018","o020","o023","o027","o029","o031","o042","o043","o045","o053","o058","o062","o066","o072","sc001","st002","st003","st004","st005","st006","st007","st008");
	$ch_arr = array("c03","c05","c07","c09");
	$url = "http://providers3.ubc.co.th/streamingproviderv2";
	$body = array(
		"uid" => "healthcheckv2",
		"sessionid" => null,
		"appid" => "trueidv2",
		"channelid" => $ch_arr[array_rand($ch_arr)],
		"langid" => "th",
		"streamlvl" => "auto",
		"type" => "live",
		"stime" => null,
		"duration" => null,
		"csip" => null,
		"geoblock" => false,
		"gps" => null,
		"agent" => "healthcheckv2",
		"visitor" => "mobile"
	);
	$exestarttime = microtime(true);
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));
	curl_setopt($curl, CURLOPT_USERAGENT, "Streaming Provider 3 ($url)");
	if(curl_errno($curl)) $ret = false;
	else {
		$ret = curl_exec($curl);
		$ret = json_decode($ret, true);
		$ret = $ret['result_code'];
	}
	curl_close($curl);
	$exeendtime = microtime(true);
	$exetime = $exeendtime - $exestarttime;
	return array("value" => $ret, "exetime" => $exetime);
}

if (@$_REQUEST['param'] == "redis") {
	$redis = is_establish_redis();
	echo number_format($redis["exetime"], 6);
}
else if (@$_REQUEST['param'] == "balancer") {
	$lbl = is_establish_balancer();
	echo number_format($lbl["exetime"], 6);
}
else if (@$_REQUEST['param'] == "service") {
	$sp3 = is_establish_service();
	echo number_format($sp3["exetime"], 6);
}
else {
	$redis = is_establish_redis();
	$lbl = is_establish_balancer();
	$sp3 = is_establish_service();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Health Check</title>
<style type="text/css">
html * {
  font-style: normal;
  font-family: arial, verdana, tahoma;
  font-size: 12px;
}

table {
  border-collapse: collapse;
}

th, td {
  padding: 2px 3px;
}

th {
  background-color: #000;
  color: #fff;
}

td {
  border-bottom: 1px solid #ccc;
}

tr.bg {
  background-color: #f0f0f0;
}
</style>
</head>
<body>
  <table width="100%">
    <tr>
      <td width="50%"><b>Service Name:</b> http://providers3.ubc.co.th</td>
      <td><b>Date/Time:</b> <?php echo date("Y-m-d H:i:s"); ?></td>
    </tr>
    <tr>
      <td><b>Module Name:</b> web</td>
      <td></td>
    </tr>
  </table>
  <p>redis, loadbalance</p>
  <table width="100%">
    <tr>
      <th>No.</th>
      <th>Description</th>
      <th>Type</th>
      <th>Param</th>
      <th>Status</th>
      <th>Value</th>
      <th>Solution</th>
    </tr>
      <tr class="bg">
      <td>1</td>
      <td>Connect to Redis, host: 10.18.19.127</td>
      <td>Redis</td>
      <td>redis</td>
      <td><?php echo ($redis["value"] ? "OK" : "ERROR"); ?></td>
      <td><?php echo number_format($redis["exetime"], 6); ?></td>
      <td></td>
    </tr>
      <tr class="bg">
      <td>3</td>
      <td>Call Balancer API - http://server_loadbalan:8080/lalaynya.php</td>
      <td>Balancer</td>
      <td>balancer</td>
      <td><?php echo ($lbl["value"] ? "OK" : "ERROR"); ?></td>
      <td><?php echo number_format((float)$lbl["exetime"], 6); ?></td>
      <td></td>
    </tr>
      <tr class="bg">
      <td>3</td>
      <td>Request /streamingprovidersv2 - http://provider3.ubc.co.th/streamingprovidersv2</td>
      <td>Streaming Provider</td>
      <td>service</td>
      <td><?php echo ($sp3["value"]); ?></td>
      <td><?php echo number_format((float)$sp3["exetime"], 6); ?></td>
      <td></td>
    </tr>
    </table>
  <?php if ($redis["value"] && $lbl["value"] && ($sp3["value"] == 420 || $sp3["value"] == 200)) echo "THIS_PAGE_IS_COMPLETELY_LOADED"; ?></body>
</html>
<?php } ?>
