<?
require_once __DIR__."/spapi/config.req.php";

class Hestia {
    private $redis;

    public function __Construct () {
        $this->redis = new Redis();
    }

    public function livemaphealth () {
        $redis_score = 0;
        # livemapping redis
        $focus = array(1, 3, 6);
        try {
            $this->redis->connect(SPRDHOST, SPRDPORT);
            $keyspaces = $this->redis->info("keyspace");
            foreach ($focus as $db) if (!isset($keyspaces["db".$db])) $redis_score += $db;
            $this->redis->close();
        }
        catch (Expection $e) {
            $redis_score += 50;
            $error_message = $this->getMessage(__FUNCTION__, $e);
            file_put_contents(LOGDIR."/healthcheck_error_". date("Ymd_H") .".log", $error_message, FILE_APPEND);
        }
        return $redis_score;
    }

    public function bizconfhealth () {
        $redis_score = 0;
        # bizconfig redis
        $focus = array(3);
        try {
            $this->redis->connect(SPRDHOST, BZRDPORT);
            $keyspaces = $this->redis->info("keyspace");
            foreach ($focus as $db) if (!isset($keyspaces["db".$db])) $redis_score += $db;
            $this->redis->close();
        }
        catch (Expection $e) {
            $redis_score += 50;
            $error_message = $this->getMessage(__FUNCTION__, $e);
            file_put_contents(LOGDIR."/healthcheck_error_". date("Ymd_H") .".log", $error_message, FILE_APPEND);
        }
        return $redis_score;
    }

    public function lbhealth () {
        $redis_score = 0;
        # livemapping redis
        $focus = array(8);
        try {
            $this->redis->connect(SPRDHOST, LBRDPORT);
            $keyspaces = $this->redis->info("keyspace");
            foreach ($focus as $db) if (!isset($keyspaces["db".$db])) $redis_score += $db;
            $this->redis->close();
        }
        catch (Expection $e) {
            $redis_score += 50;
            $error_message = $this->getMessage(__FUNCTION__, $e);
            file_put_contents(LOGDIR."/healthcheck_error_". date("Ymd_H") .".log", $error_message, FILE_APPEND);
        }
        return $redis_score;
    }

    public function apihealth () {
        $channelids = array("ht111");
        $appid = array("trueid", "antv1");
        $streamlvl = array("auto");
        $type = array("live", "timeshift", "catchup");
        $uid = "testuid";
        $sessionid = "testsess";
    }

    public function connectionhealth () {
        $redis_score = 0;
        # livemapping redis
        try {
            $this->redis->connect(SPRDHOST, SPRDPORT);
            $resp = $this->redis->ping();
            $this->redis->close();
        }
        catch (Expection $e) {
            $redis_score += 50;
            $error_message = $this->getMessage(__FUNCTION__, $e);
            file_put_contents(LOGDIR."/healthcheck_error_". date("Ymd_H") .".log", implode("  ", $log_arr), FILE_APPEND);
        }
        # cdnpool redis
        try {
            $this->redis->connect(SPRDHOST, LBRDPORT);
            $resp = $this->redis->ping();
            $this->redis->close();
        }
        catch (Expection $e) {
            $redis_score += 50;
            $error_message = $this->getMessage(__FUNCTION__, $e);
            file_put_contents(LOGDIR."/healthcheck_error_". date("Ymd_H") .".log", implode("  ", $log_arr), FILE_APPEND);
        }
        return $redis_score;
    }

    private function getMessage ($functionname, $exception) {
        $error_message = implode("  ", array(date("Y/m/d H:i:s"), "[".strtoupper($functionname)."]", $exception->getMessage(), "\n"));
    }
}
var_dump((SPRDHOST));
var_dump(gethostbyname(SPRDHOST));
$healthcheck = new Hestia();
$score['connection_score'] = $healthcheck->connectionhealth();
$score['bizconfig_score'] = $healthcheck->bizconfhealth();
$score['livemapping_score'] = $healthcheck->livemaphealth();
$score['cdnprofile_score'] = $healthcheck->lbhealth();
var_dump(implode("-", $score));
echo "Redis connection error code summary >> ". $score['connection_score'] ."\n";
echo "Biz config error code summary >> ". $score['bizconfig_score'] ."\n";
echo "Livemapping error code summary >> ". $score['livemapping_score'] ."\n";
echo "CDN profile error code summary >> ". $score['cdnprofile_score'] ."\n";