#!/bin/bash

DATE=`date '+%Y-%m-%d %H:%M:%S'`
FILEDATE=`date '+%Y%m%d_%H'`
FILEPATH="/www/webapps/providers_v3/ext/tools/logs/traceroutelog_$FILEDATE.log"
echo "**************************  $DATE ********************************" >> $FILEPATH
tcptraceroute server_loadbalan 8080 >> $FILEPATH 2>&1
echo "********************************************************************************" >> $FILEPATH
