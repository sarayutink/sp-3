<?
require_once dirname(dirname(__DIR__)) .'/spmiddleware/libraries/Curlcstm.php';
require_once dirname(dirname(__DIR__)) .'/spmiddleware/libraries/Mwredis.php';
 
class Streamingconfigdistributor {
	private $obms_api_url = "http://ottstreamingprovider.ubc.co.th/StreamProvide/getStreamByChannelCode";
	private $redis;
	private $ch_db = 1;
	private $type_arr = array('live' => "live", 'ts' => "timeshift", 'cu' => "catchup");
	private $device_arr = array('m' => "mobile", 's' => "stb", 'w' => "web");
	private $channel_file = __DIR__ .'/channels.list';
	private $channel;
	private $index_file = __DIR__ .'/index.txt';
	private $index;
	
	public function __construct () {
		// $this->redis = new Mwredis('172.22.162.177', 6380);
		$this->redis = new Mwredis('172.22.162.235', 6380);
		// $this->redis = new Mwredis('172.22.163.28', 6380);
		var_dump($this->redis);
		$this->channel = explode(";", trim(file_get_contents($this->channel_file)));
		// file_put_contents($this->index_file, count($this->channel));
		$this->index = file_get_contents($this->index_file);
		// $this->index = count($this->channel);
	}
	
	public function distributeAllConfig () {
		foreach ($this->channel as $channel) {
			$this->distributeConfig($channel);
			sleep(1);
		}
	}
	
	public function distributeConfig ($channel = false) {
		// echo $channel ." ====>\n";
		$json_obj = Curlcstm::establishServer($this->obms_api_url. "?channel_code={$channel}&profile_name=SP");
		// var_dump($json_obj);
		if (!$json_obj) {
			file_put_contents(__DIR__ ."/logs/configdistributor_". date("Ymd") .".log", "[ERROR] Channel '{$channel}' is null.\n", FILE_APPEND);
			break;
		}
		// echo $json_obj ."\n";
		try {
			$this->json_obj = json_decode($json_obj);
			$xml = $this->transformJson();
			if (!is_null($xml)) {
				$this->setConfig2Redis($channel, $xml);
				// $this->setNextIndex($channel);
				file_put_contents(__DIR__ ."/logs/configdistributor_". date("Ymd") .".log", "[SUCCES] Update '{$channel}' stream mapping : ".$xml."\n", FILE_APPEND);
				file_put_contents("/www/webapps/providers_v3/spmiddleware/assets/streamingproviderv2/channels/{$channel}.channel", $xml);
				echo  $xml."\n";
			}
			else file_put_contents(__DIR__ ."/logs/configdistributor_". date("Ymd") .".log", "[ERROR] Channel '{$channel}' is null.\n", FILE_APPEND);
			// else var_dump($e->getMessage());
		}
		catch( Exception $e ) {
			file_put_contents(__DIR__ ."/logs/configdistributor_". date("Ymd") .".log", "[ERROR] Caught exception : ".$e->getMessage()."\n", FILE_APPEND);
			// var_dump($e->getMessage());
		}
	}
	
	private function transformJson () {
		if ($this->json_obj->status == 200) {
			foreach ($this->json_obj->language as $lang => $lang_obj) {
				$chk_dup_arr = array();
				$profile_str = "";
				foreach ($lang_obj as $row) {
					$stream_lvl_arr = $this->separateProfile($row->stream_level);
					if ($stream_lvl_arr <> false && !in_array($stream_lvl_arr, $chk_dup_arr)) {
						$chk_dup_arr[] = $stream_lvl_arr;
						$profile_child_str = '<smil>'. $row->smil .'</smil>';
						$profile_child_str .= '<appinst>'. $row->stream_level != "black_out" ? $row->app : "liveedge_app" .'</appinst>';
						$profile_child_str .= '<group>'. $row->server_group .'</group>';
						$profile_str .= '<profile streamlevel="'. strtolower($stream_lvl_arr['streamlevel']) .'" device="'. strtolower($stream_lvl_arr['device']) .'" type="'. strtolower($stream_lvl_arr['type']) .'">'. $profile_child_str .'</profile>';
					}
				}
				@$xml_str .= '<language id="'. strtolower($lang) .'">'. $profile_str .'</language>';
			}
			$xml_str = '<streamprofile>'. $xml_str .'</streamprofile>';
				
			return $xml_str;
		}
		else {
			return null;
		}
	}
	
	private function separateProfile ($streamlevel, $catchup = false) {
		$tmp_arr = explode("_", $streamlevel);
		if ($streamlevel == "black_out") {
			$ret_arr['streamlevel'] = "blackout";
			$ret_arr['type'] = "blackout";
			$ret_arr['device'] = "m";
			return $ret_arr;
		}
		elseif ($streamlevel == "rtsp") {
			return false;
		}
		else {
			if (!$catchup) {
				if (count($tmp_arr) > 1) {
					$ret_arr['streamlevel'] = $tmp_arr[0];
					$ret_arr['type'] = $this->type_arr[$tmp_arr[1]];
					$ret_arr['device'] = $this->device_arr[$tmp_arr[2]];
					return $ret_arr;
				}
				else return false;
			}
			else {
				if (count($tmp_arr) > 1) {
					$ret_arr['streamlevel'] = $tmp_arr[0];
					$ret_arr['type'] = $this->type_arr[$tmp_arr[1]];
					$ret_arr['device'] = $this->device_arr[$tmp_arr[2]];
					return $ret_arr;
				}
				else return false;
			}
		}
	}
	
	private function setConfig2Redis ($channel, $xml) {
		$this->redis->setRedis($channel, $xml, $this->ch_db);
	}
	
	private function setNextIndex ($channel) {
		if (!$channel) {
			$this->index += 1;
			file_put_contents($this->index_file, ($this->index != count($this->channel) ? $this->index : 0));
		}
	}
}

$confdis = new Streamingconfigdistributor();
$confdis->distributeAllConfig();
