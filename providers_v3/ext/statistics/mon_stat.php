<?
$GLOBALS['log_path'] = '/www/webapps/providers_v3/logs/result_[Ymd_H].log';
$GLOBALS['log_count'] = array();
function readLogFile ($filepath) {
	## read log file
	$handle = @fopen($filepath, "r");
	if ($handle) {
		while (($buffer = fgets($handle, 4096)) !== false) {
			$row = explode("  ", $buffer);
			$param = countMinData($row);
			if ($param['balancer'] == "lalaynya") @$GLOBALS['log_count'][$param['date']][$param['result_code']] += 1;
		}
		fclose($handle);
	}
	else echo $filepath ." cannot read!!\n";
}

function countMinData ($row) {
	foreach ($row as $col) {
		if (strpos($col, "date:") === 0) @$param['date'] = str_replace("date:", "", $col);
		elseif (strpos($col, "result_code:") === 0) @$param['result_code'] = str_replace("result_code:", "", $col);
		elseif (strpos($col, "balancer:") === 0) @$param['balancer'] = str_replace("balancer:", "", $col);
	}
	return (isset($param) ? $param : false);
}

function main ($date, $time, $opt = null) {
	## arrange file time
	$timestamp = strtotime($date ." ". $time);
	$filepath = str_replace("[Ymd_H]", date("Ymd_H", $timestamp), $GLOBALS['log_path']);
	echo $filepath;
	//readLogFile($filepath);
}

if (!count(debug_backtrace())) {
    main($argv[1], $argv[2], $argv[3]);
	// foreach ($GLOBALS['log_count'] as $date => $results) {
		// foreach ($results as $code => $count ) {
			// echo $date ."\t". $code ."\t". $count . PHP_EOL;
		// }
	// }
}