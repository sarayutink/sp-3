<?
class RedisScope {
	public static function getGroupMembers () {
		$redis = new Redis();
		$hosts = array("10.18.19.157");
		$port = "6383";
		$dbs = array(1, 2, 3, 6, 7, 8, 11, 12, 13);
		
		$result = array();
		foreach ($hosts as $index => $host) {
			foreach ($dbs as $db) {
				$redis->connect($host, $port);
				$redis->select($db);
				// $result[$index][$db] = $redis->keys("*");
				$result[$db] = $redis->keys("*");
				$redis->close();
			}
		} 
		unset($redis);
		
		// $return = array();
		// foreach ($dbs as $db) $return[$db] = array_intersect($result[0][$db], $result[1][$db]);
		
		return $result;
	}
	
	public static function setGroupMember ($members) {
		$redis = new Redis();
		$hosts = array("10.18.19.98", "10.18.19.96");
		$port = "6380";
		$db = "2";
		
		foreach ($hosts as $index => $host) {
			foreach ($members as $key => $member) {
				// $redis->connect($host, $port);
				// $redis->select($db);
				// $sprd = $redis->get($key);
				// $redis->close();
				
				$lbrd = implode(",", $member);
				
				// if (self::compareMembers($lbrd, $sprd)) {
					$redis->connect($host, $port);
					$redis->select($db);
					$sprd = $redis->set($key, $lbrd);
					$redis->close();
					file_put_contents("/www/webapps/sp3/providers_v3/spmiddleware/assets/streamingproviderv2/groups/{$key}.group", $lbrd);
					
				// }
			}
		} 
		unset($redis);
	}
	
	private static function compareMembers ($needle, $compare) {
		// $diff = array($needle, $compare);
		// return count($diff) == 0;
		return true;
	}
}

$members = (RedisScope::getGroupMembers());
RedisScope::setGroupMember($members);