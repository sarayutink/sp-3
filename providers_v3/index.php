<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response; 

require_once __DIR__ .'/vendor/autoload.php';

$app = new \Slim\App;
$app->get('/version', function (Request $request, Response $response) {
	$response->withoutHeader('Allow');
	$return = array("response" => 200, "version" => "3.2.8", "release date" => "September 11, 2019", "powered by" => "Streaming application, TDMP.", "B64" => "bmV3cmVsaWMgYWdlbnQgZXJyb3IgbWVzc2FnZS4=", "checksum" => hash_file("sha256", dirname(__DIR__)."/providerschecksum.log"));
	$response->withJson($return);
	header("Content-Type: application/json");
	return $response;
});

$app->post('/streamingprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Streamingprovider.php';
	// $response->withoutHeader('Allow');
	$return = Streamingprovider::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	header("Access-Control-Allow-Origin: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/streamingproviderv2', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Streamingprovider2.php';
	// $response->withoutHeader('Allow');
	$return = Streamingprovider2::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	$return['version'] = "3.0";
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/radiostreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Radiostreamprovider.php';
	// $response->withoutHeader('Allow');
	$return = Radiostreamprovider::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	$return['version'] = "3.0";
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/airstreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Streamingprovider2.php';
	// $response->withoutHeader('Allow');
	$return = Streamingprovider2::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/priviledgestreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Priviledgestreamprovider.php';
	// $response->withoutHeader('Allow');
	$return = Priviledgestreamprovider::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/vodstreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Vodstreamprovider.php';
	// $response->withoutHeader('Allow');
	$return = Vodstreamprovider::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/vodstreamprovider_test', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Vodstreamprovider_test.php';
	// $response->withoutHeader('Allow');
	$return = Vodstreamprovider_test::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	print(json_encode($return));
	exit;
	// return $response;
});

$app->post('/tvodstreamprovider', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Tvodstreamprovider.php';
	$return = Tvodstreamprovider::provide($request, $response);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	// var_dump($return);
	exit;
	// return $response;
});

$app->post('/eventstreamprovider', function (Request $request, Response $response) {
	// require_once __DIR__ .'/spmiddleware/Eventstreamprovider.php';
	$response->withoutHeader('Allow');
	// $return = Eventstreamprovider::provide($request, $response);
	// $response->withJson($return);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	// echo __DIR__ .'/logs/result_'. date("Ymd_h") .".log";
	// file_put_contents(__DIR__ .'/logs/result_'. date("Ymd_H") .".log",  serialize(json_decode($request->getBody(), true)) ." >> ". serialize($return) ."\n", FILE_APPEND);
	// print(json_encode($return));
	print(json_encode(array("result_code" => 404, "result" => "/eventstreamprovider is not support on http://providers3.ubc.co.th")));
	exit;
	// return $response;
});

########## test ###############

$app->post('/streamingprovider_test', function (Request $request, Response $response) {
	require_once __DIR__ .'/spmiddleware/Streamingprovider_test.php';
	$return = Streamingprovider_test::provide($request, $response);
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	exit;
});

$app->post('/v3/livestream', function () {
	require_once __DIR__ .'/spapi/Livestream.php';
	$return = Livestream::provide();
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	exit;
});

$app->post('/v3/vodstream', function () {
	require_once __DIR__ .'/spapi/Vodstream.php';
	$return = Vodstream::provide();
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	exit;
});


$app->post('/v3/livestreamstg', function () {
	require_once __DIR__ .'/spapi/Livestreamstg.php';
	$return = Livestreamstg::provide();
	header("Content-Type: application/json");
	header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	exit;
});

$app->post('/v3/vodstreamstg', function () {
	require_once __DIR__ .'/spapi/Vodstreamstg.php';
	$return = Vodstreamstg::provide();
	header("Content-Type: application/json");
	// header("Access-Control-Allow-Origin: *");
	print(json_encode($return));
	exit;
});

$app->run();
