<?php
$return = array("response" => 200, "version" => "3.2.8", "release date" => "September 11, 2019", "powered by" => "Streaming application, TDMP.", "B64" => "bmV3cmVsaWMgYWdlbnQgZXJyb3IgbWVzc2FnZS4=", "checksum" => hash_file("sha256", dirname(__DIR__)."/providerschecksum.log"));
echo json_encode($return);
header("Content-Type: application/json");
?>
